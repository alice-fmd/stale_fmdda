dnl
<<<<<<< acinclude.m4
dnl $Id: acinclude.m4,v 1.8 2009-02-09 23:12:32 hehi Exp $
=======
dnl $Id: acinclude.m4,v 1.8 2009-02-09 23:12:32 hehi Exp $
>>>>>>> 1.7
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DEBUG],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
  AC_MSG_CHECKING(whether to make debug objects)
  AC_ARG_ENABLE(debug,
    [AC_HELP_STRING([--enable-debug],[Enable debugging symbols in objects])])
  if test "x$enable_debug" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-g,,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-g,,'`
  else
    case $CXXFLAGS in
    *-g*) ;;
    *)    CXXFLAGS="$CXXFLAGS -g" ;;
    esac
    case $CFLAGS in
    *-g*) ;;
    *)    CFLAGS="$CFLAGS -g" ;;
    esac
  fi
  AC_MSG_RESULT($enable_debug 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_OPTIMIZATION],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])

  AC_ARG_ENABLE(optimization,
    [AC_HELP_STRING([--enable-optimization],[Enable optimization of objects])])

  AC_MSG_CHECKING(for optimiztion level)

  changequote(<<, >>)dnl
  if test "x$enable_optimization" = "xno" ; then
    CFLAGS=`echo   $CFLAGS   | sed 's,-O\([0-9][0-9]*\|\),,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-O\([0-9][0-9]*\|\),,'`
  elif test "x$enable_optimization" = "xyes" ; then
    case $CXXFLAGS in
    *-O*) ;;
    *)    CXXFLAGS="$CXXFLAGS -O2" ;;
    esac
    case $CFLAGS in
    *-O*) ;;
    *)    CFLAGS="$CXXFLAGS -O2" ;;
    esac
  else
    CFLAGS=`echo   $CFLAGS   | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
    CXXFLAGS=`echo $CXXFLAGS | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
  fi
  changequote([, ])dnl
  AC_MSG_RESULT($enable_optimization 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_PROFILING],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
	
  AC_ARG_ENABLE([profiling],
	        [AC_HELP_STRING([--enable-profiling],
			        [Compile code to enable profiling])],
                [],[enable_profiling=no])
  AC_MSG_CHECKING([whether to enable profiling])
  if test "x$enable_profiling" = "xyes" ; then 
    CFLAGS="$CFLAGS -pg" 
    CXXFLAGS="$CXXFLAGS -pg"
    LDFLAGS="$LDFLAGS -pg"
  fi
  AC_MSG_RESULT([$enable_profiling])
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_STRICT],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
	
  AC_ARG_ENABLE([strict],
	        [AC_HELP_STRING([--enable-strict],
			        [Require strictly correct code])],
                [],[enable_strict=no])
  AC_MSG_CHECKING([whether require strictly correct code])
  if test "x$enable_strict" = "xyes" ; then 
    CFLAGS="$CFLAGS -Wall -Werror -pedantic -ansi" 
    # Cannot use `-pedantic' due to use of `long long'
    CXXFLAGS="$CXXFLAGS -Wall -Werror -ansi"
  fi
  AC_MSG_RESULT([$enable_strict ($CFLAGS)])
])
  
dnl ==================================================================
dnl
dnl Macro to check for thread flags
dnl
dnl AC_PTHREAD([ACTION-IF-FOUND[, ACTION-IF-NOT-FOUND]])
dnl
AC_DEFUN([AC_PTHREAD], 
[
  AC_REQUIRE([AC_CANONICAL_HOST])
  AC_LANG_SAVE
  AC_LANG_C
  ac_pthread_ok=no

  if test x"$PTHREAD_LIBS$PTHREAD_CFLAGS" != x; then
    save_CFLAGS="$CFLAGS"
    CFLAGS="$CFLAGS $PTHREAD_CFLAGS"
    save_LIBS="$LIBS"
    LIBS="$PTHREAD_LIBS $LIBS"
    AC_MSG_CHECKING([for pthread_join in LIBS=$PTHREAD_LIBS with CFLAGS=$PTHREAD_CFLAGS])
    AC_TRY_LINK_FUNC(pthread_join, ac_pthread_ok=yes)
    AC_MSG_RESULT($ac_pthread_ok)
    if test x"$ac_pthread_ok" = xno; then
      PTHREAD_LIBS=""
      PTHREAD_CFLAGS=""
    fi
    LIBS="$save_LIBS"
    CFLAGS="$save_CFLAGS"
  fi

  ac_pthread_flags="pthreads none -Kthread -kthread -lthread -pthread -pthreads -mthreads pthread --thread-safe -mt pthread-config"

  # The ordering *is* (sometimes) important.  
  case "${host_cpu}-${host_os}" in
  *solaris*)
    ac_pthread_flags="-pthreads pthread -mt -pthread $ac_pthread_flags"
    ;;
  esac

  if test x"$ac_pthread_ok" = xno; then
    PTHREAD_CFLAGS=
    PTHREAD_LDFLAGS=
    PTHREAD_LIBS=
    for flag in $ac_pthread_flags; do
      case $flag in
      none) AC_MSG_CHECKING([whether pthreads work without any flags]) ;;
      -*)   AC_MSG_CHECKING([whether pthreads work with $flag])
            PTHREAD_CFLAGS="$flag"
            ;;
      -pthread)   AC_MSG_CHECKING([whether pthreads work with linker $flag])
            PTHREAD_LDFLAGS="$flag"
            ;;
      pthread-config)
	AC_CHECK_PROG(ac_pthread_config, pthread-config, yes, no)
	if test x"$ac_pthread_config" = xno; then continue; fi
	PTHREAD_CFLAGS="`pthread-config --cflags`"
	PTHREAD_LIBS="`pthread-config --ldflags` `pthread-config --libs`"
	;;
      *) AC_MSG_CHECKING([for the pthreads library -l$flag]) 
         PTHREAD_LIBS="-l$flag"
         ;;
      esac

      save_LIBS="$LIBS"
      save_CFLAGS="$CFLAGS"
      save_LDFLAGS="$LDFLAGS"
      LIBS="$PTHREAD_LIBS $LIBS"
      CFLAGS="$CFLAGS $PTHREAD_CFLAGS"
      LDFLAGS="$LDFLAGS $PTHREAD_LDFLAGS"
      AC_TRY_LINK([#include <pthread.h>
                   void* func() { return 0; }],
                  [pthread_t th;     
		   pthread_attr_t a; 
		   pthread_join(th, 0);
		   pthread_attr_init(&a); 
		   pthread_cleanup_push(0, 0);
                   pthread_create(&th,0,&func,0); 
		   pthread_cleanup_pop(0); ],
                  [ac_pthread_ok=yes])
      LIBS="$save_LIBS"
      CFLAGS="$save_CFLAGS"
      LDFLAGS="$save_LDFLAGS"

      AC_MSG_RESULT($ac_pthread_ok)
      if test "x$ac_pthread_ok" = xyes; then
        break;
      fi

      PTHREAD_LIBS=""
      PTHREAD_CFLAGS=""
      PTHREAD_LDFLAGS=""
    done
  fi

  if test "x$ac_pthread_ok" = xyes; then
    save_LIBS="$LIBS"
    LIBS="$PTHREAD_LIBS $LIBS"
    save_CFLAGS="$CFLAGS"
    CFLAGS="$CFLAGS $PTHREAD_CFLAGS"
    save_LDFLAGS="$LDFLAGS"
    LDFLAGS="$LDFLAGS $PTHREAD_LDFLAGS"

    # Detect AIX lossage: JOINABLE attribute is called UNDETACHED.
    AC_MSG_CHECKING([for joinable pthread attribute])
    attr_name=unknown
    for attr in PTHREAD_CREATE_JOINABLE PTHREAD_CREATE_UNDETACHED; do
      AC_TRY_LINK([#include <pthread.h>], [int attr=$attr; return attr;],
                  [attr_name=$attr; break])
    done
    AC_MSG_RESULT($attr_name)
    if test "$attr_name" != PTHREAD_CREATE_JOINABLE; then
      AC_DEFINE_UNQUOTED(PTHREAD_CREATE_JOINABLE, $attr_name,
                        [Define to necessary symbol if this constant
                         uses a non-standard name on your system.])
    fi

    AC_MSG_CHECKING([if more special flags are required for pthreads])
    flag=no
    case "${host_cpu}-${host_os}" in
    *-aix* | *-freebsd* | *-darwin*) flag="-D_THREAD_SAFE";;
    *solaris* | *-osf* | *-hpux*)    flag="-D_REENTRANT"  ;;
    esac
    AC_MSG_RESULT(${flag})
    if test "x$flag" != xno; then
      PTHREAD_CFLAGS="$flag $PTHREAD_CFLAGS"
    fi

    LIBS="$save_LIBS"
    CFLAGS="$save_CFLAGS"
    LDFLAGS="$save_LDFLAGS"

    # More AIX lossage: must compile with cc_r
    AC_CHECK_PROG(PTHREAD_CC, cc_r, cc_r, ${CC})
  else
    PTHREAD_CC="$CC"
  fi

  AC_SUBST(PTHREAD_LIBS)
  AC_SUBST(PTHREAD_CFLAGS)
  AC_SUBST(PTHREAD_CC)
  AC_SUBST(PTHREAD_LDFLAGS)

  # Finally, execute ACTION-IF-FOUND/ACTION-IF-NOT-FOUND:
  AH_TEMPLATE(HAVE_PTHREAD,
              [Define if you have POSIX threads libraries and header files.])
  if test x"$ac_pthread_ok" = xyes; then
    ifelse([$1],,AC_DEFINE(HAVE_PTHREAD,1),[$1])
        :
  else
    ac_pthread_ok=no
    $2
  fi
  AC_LANG_RESTORE
])

dnl ------------------------------------------------------------------

dnl
<<<<<<< acinclude.m4
dnl $Id: acinclude.m4,v 1.8 2009-02-09 23:12:32 hehi Exp $
=======
dnl $Id: acinclude.m4,v 1.8 2009-02-09 23:12:32 hehi Exp $
>>>>>>> 1.7
dnl $Author: hehi $
<<<<<<< acinclude.m4
dnl $Date: 2009-02-09 23:12:32 $
=======
dnl $Date: 2009-02-09 23:12:32 $
>>>>>>> 1.7
dnl
dnl Autoconf macro to check for existence or ROOT on the system
dnl Synopsis:
dnl
dnl  ROOT_PATH([MINIMUM-VERSION, [ACTION-IF-FOUND, [ACTION-IF-NOT-FOUND]]])
dnl
dnl Some examples: 
dnl 
dnl    ROOT_PATH(3.03/05, , AC_MSG_ERROR(Your ROOT version is too old))
dnl    ROOT_PATH(, AC_DEFINE([HAVE_ROOT]))
dnl 
dnl The macro defines the following substitution variables
dnl
dnl    ROOTCONF           full path to root-config
dnl    ROOTEXEC           full path to root
dnl    ROOTCINT           full path to rootcint
dnl    ROOTLIBDIR         Where the ROOT libraries are 
dnl    ROOTINCDIR         Where the ROOT headers are 
dnl    ROOTCFLAGS         Extra compiler flags
dnl    ROOTLIBS           ROOT basic libraries 
dnl    ROOTGLIBS          ROOT basic + GUI libraries
dnl    ROOTAUXLIBS        Auxilary libraries and linker flags for ROOT
dnl    ROOTAUXCFLAGS      Auxilary compiler flags 
dnl    ROOTRPATH          Same as ROOTLIBDIR
dnl
dnl The macro will fail if root-config and rootcint isn't found.
dnl
dnl Christian Holm Christensen <cholm@nbi.dk>
dnl
AC_DEFUN([ROOT_PATH],
[
  AC_ARG_WITH(rootsys,
  [  --with-rootsys          top of the ROOT installation directory],
    user_rootsys=$withval,
    user_rootsys="none")
  if test ! x"$user_rootsys" = xnone; then
    rootbin="$user_rootsys/bin"
  elif test ! x"$ROOTSYS" = x ; then 
    rootbin="$ROOTSYS/bin"
  else 
   rootbin=$PATH
  fi
  AC_PATH_PROG(ROOTCONF, root-config , no, $rootbin)
  AC_PATH_PROG(ROOTEXEC, root , no, $rootbin)
  AC_PATH_PROG(ROOTCINT, rootcint , no, $rootbin)
	
  if test ! x"$ROOTCONF" = "xno" && \
     test ! x"$ROOTCINT" = "xno" ; then 

    # define some variables 
    ROOTLIBDIR=`$ROOTCONF --libdir`
    ROOTINCDIR=`$ROOTCONF --incdir`
    ROOTCFLAGS=`$ROOTCONF --noauxcflags --cflags` 
    ROOTLIBS=`$ROOTCONF --noauxlibs --noldflags --libs`
    ROOTGLIBS=`$ROOTCONF --noauxlibs --noldflags --glibs`
    ROOTAUXCFLAGS=`$ROOTCONF --auxcflags`
    ROOTAUXLIBS=`$ROOTCONF --auxlibs`
    ROOTRPATH=$ROOTLIBDIR
	
    if test $1 ; then 
      AC_MSG_CHECKING(wether ROOT version >= [$1])
      vers=`$ROOTCONF --version | tr './' ' ' | awk 'BEGIN { FS = " "; } { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      requ=`echo $1 | tr './' ' ' | awk 'BEGIN { FS = " "; } { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      if test $vers -lt $requ ; then 
        AC_MSG_RESULT(no)
	no_root="yes"
      else 
        AC_MSG_RESULT(yes)
      fi
    fi
  else
    # otherwise, we say no_root
    no_root="yes"
  fi

  AC_SUBST(ROOTLIBDIR)
  AC_SUBST(ROOTINCDIR)
  AC_SUBST(ROOTCFLAGS)
  AC_SUBST(ROOTLIBS)
  AC_SUBST(ROOTGLIBS) 
  AC_SUBST(ROOTAUXLIBS)
  AC_SUBST(ROOTAUXCFLAGS)
  AC_SUBST(ROOTRPATH)

  if test "x$no_root" = "x" ; then 
    ifelse([$2], , :, [$2])     
  else 
    ifelse([$3], , :, [$3])     
  fi
])

dnl __________________________________________________________________
dnl
dnl AC_RCUXX([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUXX],
[
    AC_REQUIRE([AC_PTHREAD])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcuxx],
        [AC_HELP_STRING([--with-rcuxx],	[Prefix where Rcu++ is installed])],
	[],[with_rcuxx="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcuxx-url],
        [AC_HELP_STRING([--with-rcuxx-url],
		[Base URL where the Rcu++ dodumentation is installed])],
        rcuxx_url=$withval, rcuxx_url="")
    if test "x${RCUXX_CONFIG+set}" != xset ; then 
        if test "x$with_rcuxx" != "xno" ; then 
	    RCUXX_CONFIG=$with_rcuxx/bin/rcuxx-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_rcuxx" != "xno" ; then 
        AC_PATH_PROG(RCUXX_CONFIG, rcuxx-config, no)
        rcuxx_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for Rcu++ version >= $rcuxx_min_version)

        # Check if we got the script
        with_rcuxx=no    
        if test "x$RCUXX_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           RCUXX_CPPFLAGS=`$RCUXX_CONFIG --cppflags`
           RCUXX_INCLUDEDIR=`$RCUXX_CONFIG --includedir`
           RCUXX_LIBS=`$RCUXX_CONFIG --libs`
           RCUXX_LTLIBS=`$RCUXX_CONFIG --ltlibs`
           RCUXX_LIBDIR=`$RCUXX_CONFIG --libdir`
           RCUXX_LDFLAGS=`$RCUXX_CONFIG --ldflags`
           RCUXX_LTLDFLAGS=`$RCUXX_CONFIG --ltldflags`
           RCUXX_PREFIX=`$RCUXX_CONFIG --prefix`
           
           # Check the version number is OK.
           rcuxx_version=`$RCUXX_CONFIG -V` 
           rcuxx_vers=`echo $rcuxx_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           rcuxx_regu=`echo $rcuxx_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $rcuxx_vers -ge $rcuxx_regu ; then 
                with_rcuxx=yes
           fi
        fi
        AC_MSG_RESULT($with_rcuxx - is $rcuxx_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_RCUXX, [Whether we have rcuxx])
    
    
        if test "x$with_rcuxx" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
	    save_LIBS=$LIBS
	    LIBS="$LIBS $PTHREAD_LIBS $RCUXX_LIBS"
            LDFLAGS="$LDFLAGS $PTHREAD_LDFLAGS -L$RCUXX_LIBDIR"
            CPPFLAGS="$CPPFLAGS $PTHREAD_CFLAGS $RCUXX_CPPFLAGS"
     
            # Change the language 
            AC_LANG_PUSH(C++)
    
     	    # Check for a header 
            have_rcuxx_rcu_h=0
            AC_CHECK_HEADER([rcuxx/Rcu.h], [have_rcuxx_rcu_h=1])
    
            # Check the library. 
            have_librcuxx=no
            AC_MSG_CHECKING(for -lrcuxx)
            AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <rcuxx/Rcu.h>],
                                            [Rcuxx::Rcu::Open("foo")])], 
                                            [have_librcuxx=yes])
            AC_MSG_RESULT($have_librcuxx)
    
            if test $have_rcuxx_rcu_h -gt 0    && \
                test "x$have_librcuxx"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_RCUXX)
            else 
                with_rcuxx=no
            fi
            # Change the language 
            AC_LANG_POP(C++)
	    CPPFLAGS=$save_CPPFLAGS
    	    LDFLAGS=$save_LDFLAGS
	    LIBS=$save_LIBS
        fi
    
        AC_MSG_CHECKING(where the Rcu++ documentation is installed)
        if test "x$rcuxx_url" = "x" && \
    	test ! "x$RCUXX_PREFIX" = "x" ; then 
           RCUXX_URL=${RCUXX_PREFIX}/share/doc/rcuxx
        else 
    	RCUXX_URL=$rcuxx_url
        fi	
        AC_MSG_RESULT($RCUXX_URL)
    fi
   
    if test "x$with_rcuxx" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUXX_URL)
    AC_SUBST(RCUXX_PREFIX)
    AC_SUBST(RCUXX_CPPFLAGS)
    AC_SUBST(RCUXX_INCLUDEDIR)
    AC_SUBST(RCUXX_LDFLAGS)
    AC_SUBST(RCUXX_LIBDIR)
    AC_SUBST(RCUXX_LIBS)
    AC_SUBST(RCUXX_LTLIBS)
    AC_SUBST(RCUXX_LTLDFLAGS)
])
dnl
dnl EOF
dnl 


dnl __________________________________________________________________
dnl
dnl AC_READRAW([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_READRAW],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([readraw-prefix],
        [AC_HELP_STRING([--with-readraw-prefix],
		[Prefix where ReadRaw is installed])],
        readraw_prefix=$withval, readraw_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([readraw-url],
        [AC_HELP_STRING([--with-readraw-url],
		[Base URL where the ReadRaw dodumentation is installed])],
        readraw_url=$withval, readraw_url="")
    if test "x${READRAW_CONFIG+set}" != xset ; then 
        if test "x$readraw_prefix" != "x" ; then 
	    READRAW_CONFIG=$readraw_prefix/bin/readraw-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(READRAW_CONFIG, readraw-config, no)
    readraw_min_version=ifelse([$1], ,0.3,$1)
    
    # Message to user
    AC_MSG_CHECKING(for ReadRaw version >= $readraw_min_version)

    # Check if we got the script
    readraw_found=no    
    if test "x$READRAW_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       READRAW_CPPFLAGS=`$READRAW_CONFIG --cppflags`
       READRAW_INCLUDEDIR=`$READRAW_CONFIG --includedir`
       READRAW_LIBS=`$READRAW_CONFIG --libs`
       READRAW_LTLIBS=`$READRAW_CONFIG --ltlibs`
       READRAW_LIBDIR=`$READRAW_CONFIG --libdir`
       READRAW_LDFLAGS=`$READRAW_CONFIG --ldflags`
       READRAW_LTLDFLAGS=`$READRAW_CONFIG --ltldflags`
       READRAW_PREFIX=`$READRAW_CONFIG --prefix`
       
       # Check the version number is OK.
       readraw_version=`$READRAW_CONFIG -V` 
       readraw_vers=`echo $readraw_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       readraw_regu=`echo $readraw_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $readraw_vers -ge $readraw_regu ; then 
            readraw_found=yes
       fi
    fi
    AC_MSG_RESULT($readraw_found - is $readraw_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_READRAW_READER_H, 
                [Whether we have readraw/Reader.h header])
    AH_TEMPLATE(HAVE_READRAW, [Whether we have readraw])


    if test "x$readraw_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
	save_LIBS=$LIBS
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS -L$READRAW_LIBDIR"
    	CPPFLAGS="$CPPFLAGS $READRAW_CPPFLAGS"
 	LIBS="$LIBS $READRAW_LIBS"

        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_readraw_reader_h=0
        AC_CHECK_HEADER([readraw/Reader.h], [have_readraw_reader_h=1])

        # Check the library. 
        have_libreadraw=no
        AC_MSG_CHECKING(for -lreadraw)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <readraw/Reader.h>],
                        [unsigned long w; ReadRaw::Reader::SwapBytes(w)])], 
                        [have_libreadraw=yes])
        AC_MSG_RESULT($have_libreadraw)

        if test $have_readraw_reader_h -gt 0    && \
            test "x$have_libreadraw"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_READRAW_READER_H)
            AC_DEFINE(HAVE_READRAW)
        else 
            readraw_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
	CPPFLAGS=$save_CPPFLAGS
	LDFLAGS=$save_LDFLAGS
	LIBS=$save_LIBS
    fi

    AC_MSG_CHECKING(where the ReadRaw documentation is installed)
    if test "x$readraw_url" = "x" && \
	test ! "x$READRAW_PREFIX" = "x" ; then 
       READRAW_URL=${READRAW_PREFIX}/share/doc/readraw
    else 
	READRAW_URL=$readraw_url
    fi	
    AC_MSG_RESULT($READRAW_URL)
   
    if test "x$readraw_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(READRAW_URL)
    AC_SUBST(READRAW_PREFIX)
    AC_SUBST(READRAW_CPPFLAGS)
    AC_SUBST(READRAW_INCLUDEDIR)
    AC_SUBST(READRAW_LDFLAGS)
    AC_SUBST(READRAW_LIBDIR)
    AC_SUBST(READRAW_LIBS)
    AC_SUBST(READRAW_LTLIBS)
    AC_SUBST(READRAW_LTLDFLAGS)
])

dnl __________________________________________________________________
dnl
dnl AC_ALTROCC([ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ALTROCC],
[
    AC_ARG_WITH([altrocc], 
    	    [AC_HELP_STRING([--with-altrocc],
    	                    [Prefix of ALTROCC installation])])
    save_LDFLAGS=$LDFLAGS
    save_CPPFLAGS=$CPPFLAGS
    if test ! "x$with_altrocc" = "x" && test ! "x$with_altrocc" = "xno" ; then 
       LDFLAGS="$LDFLAGS -L$with_altrocc/lib/altrocc" 
       CPPFLAGS="$CPPFLAGS -I$with_altrocc/include"
    else
       with_altrocc=
    fi
    have_altrocc_compiler_h=0
    AH_TEMPLATE(HAVE_ALTROCC_COMPILER_H, [Whether we have ALTROCC header])
    AC_CHECK_HEADER([altrocc/compiler.h], [have_altrocc_compiler_h=1])
    have_libaltrocc=0
    AC_CHECK_LIB([altrocc], [RCUC_compile], [have_libaltrocc=1])
    if test $have_libaltrocc -gt 0 && \
	test $have_altrocc_compiler_h -gt 0; then 
       AC_DEFINE(HAVE_ALTROCC_COMPILER_H)
       if test ! "x$with_altrocc" = "x" ; then 
          ALTROCCLDFLAGS="-L$with_altrocc/lib"
          ALTROCCCPPFLAGS="-I$with_altrocc/include"
       fi
       with_altrocc=yes
       ALTROCCLIB=-laltrocc
    else 
       with_altrocc=no
       ALTROCCLDFLAGS=
       ALTROCCPPFLAGS=
    fi				     
    LDFLAGS="$save_LDFLAGS"
    CPPFLAGS="$save_CPPFLAGS"

    if test "x$with_altrocc" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else 
        ifelse([$2], , :, [$2])
    fi
    AC_SUBST([ALTROCCLDFLAGS])
    AC_SUBST([ALTROCCCPPFLAGS])
    AC_SUBST([ALTROCCLIB])
])
dnl
dnl EOF
dnl 
    
#
# EOF
#
dnl -*- mode: Autoconf -*- 
dnl
<<<<<<< acinclude.m4
dnl $Id: acinclude.m4,v 1.8 2009-02-09 23:12:32 hehi Exp $ 
=======
dnl $Id: acinclude.m4,v 1.8 2009-02-09 23:12:32 hehi Exp $ 
>>>>>>> 1.7
dnl  
dnl  ROOT generic rcudb framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_RCUDB([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUDB],
[
    # AC_REQUIRE([AC_RCUXX])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcudb-prefix],
        [AC_HELP_STRING([--with-rcudb-prefix],
		[Prefix where RcuDb is installed])],
        rcudb_prefix=$withval, rcudb_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcudb-url],
        [AC_HELP_STRING([--with-rcudb-url],
		[Base URL where the RcuDb dodumentation is installed])],
        rcudb_url=$withval, rcudb_url="")
    if test "x${RCUDB_CONFIG+set}" != xset ; then 
        if test "x$rcudb_prefix" != "x" ; then 
	    RCUDB_CONFIG=$rcudb_prefix/bin/rcudb-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(RCUDB_CONFIG, rcudb-config, no)
    rcudb_min_version=ifelse([$1], ,0.11,$1)
    
    # Message to user
    AC_MSG_CHECKING(for RcuDb version >= $rcudb_min_version)

    # Check if we got the script
    rcudb_found=no    
    if test "x$RCUDB_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       RCUDB_CPPFLAGS=`$RCUDB_CONFIG --cppflags`
       RCUDB_INCLUDEDIR=`$RCUDB_CONFIG --includedir`
       RCUDB_LIBS=`$RCUDB_CONFIG --libs`
       RCUDB_LTLIBS=`$RCUDB_CONFIG --ltlibs`
       RCUDB_LIBDIR=`$RCUDB_CONFIG --libdir`
       RCUDB_LDFLAGS=`$RCUDB_CONFIG --ldflags`
       RCUDB_LTLDFLAGS=`$RCUDB_CONFIG --ltldflags`
       RCUDB_PREFIX=`$RCUDB_CONFIG --prefix`

       # Check the version number is OK.
       rcudb_version=`$RCUDB_CONFIG -V` 
       rcudb_vers=`echo $rcudb_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       rcudb_regu=`echo $rcudb_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $rcudb_vers -ge $rcudb_regu ; then 
            rcudb_found=yes
       fi
    fi
    AC_MSG_RESULT($rcudb_found - is $rcudb_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_RCUDB, [Whether we have rcudb])

    if test "x$rcudb_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
	save_LIBS=$LIBS
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS -L$RCUDB_LIBDIR"
    	CPPFLAGS="$CPPFLAGS $RCUDB_CPPFLAGS"
	LIBS="$LIBS $RCUDB_LIBS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_rcudb_server_h=0
        AC_CHECK_HEADER([rcudb/Server.h], 
	                [have_rcudb_server_h=1])

        # Check the library. 
        have_librcudb=no
        AC_MSG_CHECKING(for -lrcudb)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <rcudb/Sql.h>],
                        [new RcuDb::Sql("");])], 
                        [have_librcudb=yes])
        AC_MSG_RESULT($have_librcudb)

        if test $have_rcudb_server_h -gt 0    && \
            test "x$have_librcudb"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_RCUDB)
        else 
            rcudb_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)

	LIBS=$save_LIBS
	LDFLAGS=$save_LDFLAGS
	CPPFLAGS=$save_CPPFLAGS
    fi

    AC_MSG_CHECKING(where the RcuDb documentation is installed)
    if test "x$rcudb_url" = "x" && \
	test ! "x$RCUDB_PREFIX" = "x" ; then 
       RCUDB_URL=${RCUDB_PREFIX}/share/doc/rcudb
    else 
	RCUDB_URL=$rcudb_url
    fi	
    AC_MSG_RESULT($RCUDB_URL)
   
    if test "x$rcudb_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUDB_URL)
    AC_SUBST(RCUDB_PREFIX)
    AC_SUBST(RCUDB_CPPFLAGS)
    AC_SUBST(RCUDB_INCLUDEDIR)
    AC_SUBST(RCUDB_LDFLAGS)
    AC_SUBST(RCUDB_LIBDIR)
    AC_SUBST(RCUDB_LIBS)
    AC_SUBST(RCUDB_LTLIBS)
    AC_SUBST(RCUDB_LTLDFLAGS)
])

dnl
dnl EOF
dnl 
dnl -*- mode: Autoconf -*- 
dnl
<<<<<<< acinclude.m4
dnl $Id: acinclude.m4,v 1.8 2009-02-09 23:12:32 hehi Exp $ 
=======
dnl $Id: acinclude.m4,v 1.8 2009-02-09 23:12:32 hehi Exp $ 
>>>>>>> 1.7
dnl  
dnl  ROOT generic rcugsl framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_RCUGSL([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUGSL],
[
    # AC_REQUIRE([AC_RCUXX])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcugsl-prefix],
        [AC_HELP_STRING([--with-rcugsl-prefix],
		[Prefix where Rcugsl is installed])],
        rcugsl_prefix=$withval, rcugsl_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcugsl-url],
        [AC_HELP_STRING([--with-rcugsl-url],
		[Base URL where the Rcugsl dodumentation is installed])],
        rcugsl_url=$withval, rcugsl_url="")
    if test "x${RCUGSL_CONFIG+set}" != xset ; then 
        if test "x$rcugsl_prefix" != "x" ; then 
	    RCUGSL_CONFIG=$rcugsl_prefix/bin/rcugsl-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(RCUGSL_CONFIG, rcugsl-config, no)
    rcugsl_min_version=ifelse([$1], ,0.11,$1)
    
    # Message to user
    AC_MSG_CHECKING(for RcuGsl version >= $rcugsl_min_version)

    # Check if we got the script
    rcugsl_found=no    
    if test "x$RCUGSL_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       RCUGSL_CPPFLAGS=`$RCUGSL_CONFIG --cppflags`
       RCUGSL_INCLUDEDIR=`$RCUGSL_CONFIG --includedir`
       RCUGSL_LIBS=`$RCUGSL_CONFIG --libs`
       RCUGSL_LTLIBS=`$RCUGSL_CONFIG --ltlibs`
       RCUGSL_LIBDIR=`$RCUGSL_CONFIG --libdir`
       RCUGSL_LDFLAGS=`$RCUGSL_CONFIG --ldflags`
       RCUGSL_LTLDFLAGS=`$RCUGSL_CONFIG --ltldflags`
       RCUGSL_PREFIX=`$RCUGSL_CONFIG --prefix`

       # Check the version number is OK.
       rcugsl_version=`$RCUGSL_CONFIG -V` 
       rcugsl_vers=`echo $rcugsl_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       rcugsl_regu=`echo $rcugsl_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $rcugsl_vers -ge $rcugsl_regu ; then 
            rcugsl_found=yes
       fi
    fi
    AC_MSG_RESULT($rcugsl_found - is $rcugsl_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_RCUGSL, [Whether we have rcugsl])

    if test "x$rcugsl_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
	save_LIBS=$LIBS
    	LDFLAGS="$LDFLAGS -L$RCUGSL_LIBDIR"
    	CPPFLAGS="$CPPFLAGS $RCUGSL_CPPFLAGS"
	LIBS="$LIBS $RCUGSL_LIBS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_rcugsl_histogram_h=0
        AC_CHECK_HEADER([rcugsl/Histogram.h], 
	                [have_rcugsl_histogram_h=1])

        # Check the library. 
        have_librcugsl=no
        AC_MSG_CHECKING(for -lrcugsl)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <rcugsl/Histogram.h>],
                        [new RcuGsl::Histogram(100,0,10);])], 
                        [have_librcugsl=yes])
        AC_MSG_RESULT($have_librcugsl)

        if test $have_rcugsl_histogram_h -gt 0    && \
            test "x$have_librcugsl"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_RCUGSL)
        else 
            rcugsl_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
	LDFLAGS=$save_LDFLAGS
	LIBS=$save_LIBS
	CPPFLAGS=$save_CPPFLAGS
    fi

    AC_MSG_CHECKING(where the RcuGsl documentation is installed)
    if test "x$rcugsl_url" = "x" && \
	test ! "x$RCUGSL_PREFIX" = "x" ; then 
       RCUGSL_URL=${RCUGSL_PREFIX}/share/doc/rcugsl
    else 
	RCUGSL_URL=$rcugsl_url
    fi	
    AC_MSG_RESULT($RCUGSL_URL)
   
    if test "x$rcugsl_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUGSL_URL)
    AC_SUBST(RCUGSL_PREFIX)
    AC_SUBST(RCUGSL_CPPFLAGS)
    AC_SUBST(RCUGSL_INCLUDEDIR)
    AC_SUBST(RCUGSL_LDFLAGS)
    AC_SUBST(RCUGSL_LIBDIR)
    AC_SUBST(RCUGSL_LIBS)
    AC_SUBST(RCUGSL_LTLIBS)
    AC_SUBST(RCUGSL_LTLDFLAGS)
])

dnl
dnl EOF
dnl 
