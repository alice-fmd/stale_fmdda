/*
 * $Id: Mainpage.h,v 1.3 2009-02-09 23:12:32 hehi Exp $
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */
/** @mainpage FMD DAQ Detector Algorithms

    @section content Content

    - @ref overview 
    - @ref architecture 
    - @ref hardware
    - API of @ref fmdda 
      - @ref fmdda_raw
      - @ref fmdda_env
      - @ref gains 
        - @ref gain_spectra 
      - @ref peds 
        - @ref adc_spectra 

    @section overview Overview 

    This package contains 2 programs, @ref fmdda_ped and @ref fmdda_gain
    as well as a support library and headers.  

    @ref fmdda_ped reads in raw data and extracts the pedestal and
    noise value for each timebin.  It writes a file to the DAQ File
    eXchange Server and an entry in the DAQ log book.  It also outputs
    a file per ALTRO channel which can late be loaded into the ALTRO
    channel pattern memory for on-line pedestal subtraction.

    @ref fmdda_gain reads in raw data from a pulser run, and extracs
    the gain for each strip.  The result is written to the DAQ File
    eXchange Server, and an entry in the DAQ log book is made.

    @section architecture Architecture 

    The basic class of the algorithms is FmdDA::Maker.  The two
    classes FmdDA::PedestalMaker and FmdDA::GainMaker - the 
    @e makers - derive from this.

    The makers use a hierarcal storage of spectra build up from
    specialisations of the class template FmdDA::Container.  The
    hierarchies contain data relevant for the particular maker.  For
    example, the classes in @ref gain_spectra contain one ADC spectra
    per strip per pulser value.  

    Each maker fits functions to the spectra to the relevant data.
    The pedestal maker, for example fit a Gaussian to each of the ADC
    spectra.  The gain maker first fits a Gaussian to the each ADC
    spectra at each pulser value, and then fits a straight line to the
    pulser vs. mean distributions.  The spectra and fits are done
    using the RcuGsl wrappers around the GNU Scientific Library. 

    The data is read using the FmdDA::Reader class, and decoded into
    objecs of FmdDA::Channel class by the FmdDA::AltroDecoder class.
    The FmdDA::Reader class in turn uses ReadRaw to do the low-level
    processing of the raw data. 
    
    There's a number of utility classes. FmdDA::FXS for example, takes
    care of copying the data to the File eXchange Server and write
    entries in the DAQ log book.  The class FmdDA::ProgressMeter
    writes friendly messages to the screen.  The database
    communication is done using the abstraction layer RcuDb.  

    @section hardware Hardware settings. 

    If the programs are linked against the Rcuxx library, they can
    read parameters directly from the hardware.  If not, the program
    rely on the user to specify appropriate command line parameters. 
    
*/

#error Not for compilation
//
// EOF
//
