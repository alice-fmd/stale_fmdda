dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: fmdda.m4,v 1.2 2009-02-09 23:12:33 hehi Exp $ 
dnl  
dnl  ROOT generic fmdda framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_FMDDA([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_FMDDA],
[
    AC_REQUIRE([ROOT_PATH])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([fmdda-prefix],
        [AC_HELP_STRING([--with-fmdda-prefix],
		[Prefix where FmdDA is installed])],
        fmdda_prefix=$withval, fmdda_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([fmdda-url],
        [AC_HELP_STRING([--with-fmdda-url],
		[Base URL where the FmdDA dodumentation is installed])],
        fmdda_url=$withval, fmdda_url="")
    if test "x${FMDDA_CONFIG+set}" != xset ; then 
        if test "x$fmdda_prefix" != "x" ; then 
	    FMDDA_CONFIG=$fmdda_prefix/bin/fmdda-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(FMDDA_CONFIG, fmdda-config, no)
    fmdda_min_version=ifelse([$1], ,0.11,$1)
    
    # Message to user
    AC_MSG_CHECKING(for FmdDA version >= $fmdda_min_version)

    # Check if we got the script
    fmdda_found=no    
    if test "x$FMDDA_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       FMDDA_CPPFLAGS=`$FMDDA_CONFIG --cppflags`
       FMDDA_INCLUDEDIR=`$FMDDA_CONFIG --includedir`
       FMDDA_LIBS=`$FMDDA_CONFIG --libs`
       FMDDA_LTLIBS=`$FMDDA_CONFIG --ltlibs`
       FMDDA_LIBDIR=`$FMDDA_CONFIG --libdir`
       FMDDA_LDFLAGS=`$FMDDA_CONFIG --ldflags`
       FMDDA_LTLDFLAGS=`$FMDDA_CONFIG --ltldflags`
       FMDDA_PREFIX=`$FMDDA_CONFIG --prefix`
       FMDDA_READER=`$FMDDA_CONFIG --have-reader`
       FMDDA_LOWLEVEL=`$FMDDA_CONFIG --have-lowlevel`

       # Check the version number is OK.
       fmdda_version=`$FMDDA_CONFIG -V` 
       fmdda_vers=`echo $fmdda_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       fmdda_regu=`echo $fmdda_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $fmdda_vers -ge $fmdda_regu ; then 
            fmdda_found=yes
       fi
    fi
    AC_MSG_RESULT($fmdda_found - is $fmdda_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_FMDDA_CHANNEL_H, 
                [Whether we have fmdda/Channel.h header])
    AH_TEMPLATE(HAVE_FMDDA, [Whether we have fmdda])


    if test "x$fmdda_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS -L$FMDDA_LIBDIR $FMDDA_LIBS -L$ROOTLIBDIR $ROOTLIBS $ROOTAUXLIBS"
    	CPPFLAGS="$CPPFLAGS $FMDDA_CPPFLAGS $ROOTCFLAGS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_fmdda_channel_h=0
        AC_CHECK_HEADER([fmdda/Channel.h], [have_fmdda_channel_h=1])

        # Check the library. 
        have_libfmdda=no
        AC_MSG_CHECKING(for -lfmdda)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <fmdda/Channel.h>],
                        [new FmdDA::Channel;])], 
                        [have_libfmdda=yes])
        AC_MSG_RESULT($have_libfmdda)

        if test $have_fmdda_channel_h -gt 0    && \
            test "x$have_libfmdda"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_FMDDA)
        else 
            fmdda_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
    fi

    AC_MSG_CHECKING(where the FmdDA documentation is installed)
    if test "x$fmdda_url" = "x" && \
	test ! "x$FMDDA_PREFIX" = "x" ; then 
       FMDDA_URL=${FMDDA_PREFIX}/share/doc/fmdda/html
    else 
	FMDDA_URL=$fmdda_url
    fi	
    AC_MSG_RESULT($FMDDA_URL)
   
    if test "x$fmdda_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(FMDDA_URL)
    AC_SUBST(FMDDA_PREFIX)
    AC_SUBST(FMDDA_CPPFLAGS)
    AC_SUBST(FMDDA_INCLUDEDIR)
    AC_SUBST(FMDDA_LDFLAGS)
    AC_SUBST(FMDDA_LIBDIR)
    AC_SUBST(FMDDA_LIBS)
    AC_SUBST(FMDDA_LTLIBS)
    AC_SUBST(FMDDA_LTLDFLAGS)
    AC_SUBST(FMDDA_READER)
    AC_SUBST(FMDDA_LOWLEVEL)
])

dnl
dnl EOF
dnl 
