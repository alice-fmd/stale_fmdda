/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jan 16 18:27:59 2007
    @brief   Analyse a "pedestal" run. 
*/
#include "config.h"
#include "Options.h"
#include "PedestalMaker.h"
#include "FXS.h"
#include "DebugGuard.h"
#include <string>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#ifdef HAVE_RCUXX
# include <rcuxx/Rcu.h>
#endif
#ifdef HAVE_DAQDA_H
# include "daqDA.h"
#endif
//____________________________________________________________________
/** @ingroup peds
    @anchor fmdda_ped
    @brief Main program for @b fmdda_ped
    
    This program reads in raw pedestal data, and extracts the
    pedestals and nose for each time bin. It writes a file (by default
    @c gains.csv) of Comma Separated Values to DAQ File eXchange
    Server, and makes an entry in the DAQ log book.  It also write one
    file per ALTRO channel to the designated directory (by default,
    the current directory).  The content of these files can later be
    loaded into the ALTRO channels pattern memory for on-line pedestal
    subtraction. 
*/
int
main(int argc, char** argv) 
{
  // General options 
  Option<bool>        hOpt('h',"help",    "\tThis help", false, false); 
  Option<bool>        vOpt('v',"version", "\tShow version",false,false);
  // Reader options 
  Option<long>        nOpt('n',"events", "\tNumber of events", -1);
  Option<unsigned>    sOpt('s',"skip",   "\t# events to skip", 0);
  // FMD options 
  Option<unsigned>    OOpt('O',"over-sampling", "Over sampling ratio", 4);
  Option<unsigned>    mOpt('m',"first-strip",   "First strip\t", 0); 
  Option<unsigned>    MOpt('M',"last-strip",    "Last strip\t", 127); 
  //Default ddl = 3072 (our DDL from DAQ)
  Option<unsigned>    bOpt('b',"base-ddl",      "Base DDL number\t", 
			   (0xc << 8)); 
  //Default ddl = 20010
  //Option<unsigned>    bOpt('b',"base-ddl",      "Base DDL number\t", 0x4e2a); 
  Option<std::string> cOpt('c',"url", "\tHardware URL\t", "");
  // Pedestal maker options 
  Option<unsigned>    tOpt('t',"sample", "\tSample to use\t", 2);
  Option<bool>        fOpt('f',"fit",    "\tAlso fit spectra", false,false);
  Option<std::string> oOpt('o',"output", "\tOutput file name", "peds.csv");
  Option<std::string> pOpt('p',"out-dir", "Output directory", ".");
  // FXS options 
  Option<int>         rOpt('r',"run",      "\tRun number\t", 0);
  Option<std::string> dOpt('d',"detector", "Detector\t", "FMD");
  Option<std::string> ROpt('R',"role",     "\tDAQ Role\t", "Calib");
  Option<std::string> POpt('P',"path",     "\tPath to FXS\t","localhost:/tmp");
  Option<std::string> DOpt('D',"database", "Database\t", 
			   "mysql://daq:daq@localhost/DATE_LOG");
  Option<bool>        gOpt('g',"debug", "Turn on debugging\t", false, false);
  
  CommandLine cl("SOURCE");
  // General 
  cl.Add(hOpt);
  cl.Add(vOpt);
  cl.Add(gOpt);
  // Reader
  cl.Add(nOpt);
  cl.Add(sOpt);
  // FMD 
  cl.Add(OOpt);
  cl.Add(mOpt);
  cl.Add(MOpt);
  cl.Add(cOpt);
  cl.Add(bOpt);
  // Pedestal 
  cl.Add(oOpt);
  cl.Add(tOpt);
  cl.Add(fOpt);
  cl.Add(pOpt);
  // FXS 
  cl.Add(rOpt);
  cl.Add(DOpt);
  cl.Add(dOpt);
  cl.Add(ROpt);
  cl.Add(POpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
#ifdef HAVE_RCUXX
    Rcuxx::Rcu::PrintHelp(std::cout);
#endif
    return 0;
  }
  DSET(gOpt.IsSet());
  if (vOpt.IsSet()) {
    std::cout << "fmdped version " << VERSION << std::endl;
    return 0;
  }

  std::string output = oOpt;
  std::string hw     = cOpt;
  std::string outdir = pOpt;
  const FmdDA::PedestalMaker::Sources_t& sources = cl.Remain();
  if (sources.size() < 1) {
    std::cerr << "No sources specified" << std::endl;
    return 0;
  }
  FmdDA::PedestalMaker maker(sources, output.c_str(), outdir.c_str(), 
			     nOpt, sOpt, bOpt, true, true);
  maker.SetOverSample(OOpt);
  maker.SetRange(mOpt, MOpt);
  maker.SetFit(fOpt.IsSet());

  bool ret = maker.Run(hw);

  int retval = 1;
  if (ret && DOpt.Value() != "") {
    std::string url  = DOpt;
    std::string det  = dOpt;
    std::string role = ROpt;
    std::string path = POpt;
#ifdef HAVE_DAQDA_H
    retval = daqDA_FES_storeFile(output.c_str(), "pedestals");
#else
    FmdDA::FXS fxs(url, rOpt, det, role, path);
    retval = fxs.CopyFile(output, "pedestals");
#endif
  }
  if (retval != 0) std::cerr << "Pedestal Maker failed" << std::endl;

  return retval;
}

//
// EOF
//
    
	
	
	
	 

