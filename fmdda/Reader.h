// -*- mode: C++ -*-
//____________________________________________________________________
//
// $Id: Reader.h,v 1.3 2009-02-09 23:12:33 hehi Exp $
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Base class for reading channel information.
*/
#ifndef FMDDA_READER_H
#define FMDDA_READER_H
#ifndef __CCTYPE__
# include <cctype>
#endif
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef __STRING__
# include <string>
#endif

namespace ReadRaw 
{
  struct Reader;
  struct Mask;
}


namespace FmdDA
{
  struct Channel;
  struct AltroDecoder;

  /** @class Reader 
      @brief Read raw data using ReadRaw::Reader 
      @ingroup fmdda_raw */
  struct Reader
  {
    /** Return codes */
    enum {
      /** Got data */
      kData,
      /** We should skip this  */
      kSkip,
      /** No data  */
      kNoData, 
      /** End of data seen  */
      kEndOfData, 
      /** Max number of events processed */
      kMaxReached,
      /** There was an error */
      kError
    };
    
    /** Static member function to make a reader 
	@param v    Visitor 
	@param n    Number of events to read. 
	@param skip The number of events to skip. 
	@return a newly allocated reader, or 0 on error */
    Reader(AltroDecoder& v, 
	   long          n      = -1, 
	   size_t        skip   = 0) 
      : fProc(v), 
	fMaxEvents(n), 
	fSkipEvents(skip), 
	fCurrentEvent(0), 
	fIsEOD(true), 
	fReader(0), 
	fMask(0),
	fSkipChannel(false), 
	fLastEvent(0)
    {}    
    /** Set input source.
	@param src   Where to read from. 
	@param all   All events?
	@param block Blocking read?
	@return @c true on succes, @c false otherwise */
    virtual bool SetInput(const std::vector<std::string>&  src, 
			  bool all=true, bool block=true);
    
    /** Set input source.
	@param src   Where to read from. 
	@param all   All events?
	@param block Blocking read?
	@return @c true on succes, @c false otherwise */
    virtual bool SetInput(const char* src, bool all=true, bool block=true);

    /** Destructor */
    virtual ~Reader();
    
    /** Read in next event.  
	@return @c true on success, @c false otherwise */
    virtual int GetNextEvent();
    /** @return the (estimated) number of events */
    virtual long GetNumberOfEvents() const;
    /** Get the last read event number */ 
    virtual long GetEventNumber() const { return fLastEvent; }
    /** @return end of data flag */
    bool IsEOD() const { return fIsEOD; }
  protected:
    /** Set the mask 
	@param all Read all events 
	@param block Blocking reads */
    void SetMask(bool all, bool block);
    /** The channel visitor */ 
    AltroDecoder& fProc;
    /** Number of events to read */
    long fMaxEvents;
    /** Number of events to skip */
    size_t fSkipEvents;
    /** Current event number */
    unsigned long fCurrentEvent;
    /** Whether we're at the end of the data */
    bool fIsEOD;
    /** Raw reader */
    ReadRaw::Reader* fReader;
    /** The mask for the raw reader */ 
    ReadRaw::Mask*   fMask;
    /** whether we should skip the rest of this channel */ 
    bool fSkipChannel;
    /** Last event number */ 
    unsigned long fLastEvent;
  };
}

#endif  
/*
 * EOF
 */
