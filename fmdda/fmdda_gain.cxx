/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jan 16 18:27:10 2007
    @brief   Analyse a pulse calibration run 
*/
#include "config.h"
#include "GainMaker.h"
#include "Options.h"
#include "FXS.h"
#include <string>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#ifdef HAVE_RCUXX
# include <rcuxx/Rcu.h>
#endif
#ifdef HAVE_DAQDA_H
# include "daqDA.h"
#endif
//____________________________________________________________________
/** @ingroup gains 
    @anchor fmdda_gain
    @brief Main program for @b fmdda_gain
    
    This program reads in raw pulser data, and extracts the gains for
    each strip.  It writes a file (by default @c gains.csv) of Comma
    Separated Values to DAQ File eXchange Server, and makes an entry
    in the DAQ log book. 
*/
int
main(int argc, char** argv) 
{
  // General options 
  Option<bool>        hOpt('h',"help",    "\tThis help", false, false); 
  Option<bool>        vOpt('v',"version", "\tShow version",false,false);
  // FMD options 
  Option<unsigned>    OOpt('O',"over-sampling", "Over sampling ratio", 4);
  Option<unsigned>    mOpt('m',"first-strip",   "First strip\t", 0); 
  Option<unsigned>    MOpt('M',"last-strip",    "Last strip\t", 127); 
  //With default ddl = 0 - our ddl 3072 from DAQ 
  Option<unsigned>    bOpt('b',"base-ddl",      "Base DDL number\t", 3072); 
  //With default ddl = 20010
  //Option<unsigned>    bOpt('b',"base-ddl",      "Base DDL number\t", 0x4e2a); 
  Option<std::string> cOpt('c',"url",           "\tHardware URL\t", "");
  // GainMaker options 
  Option<long>        nOpt('n', "samples", "Number of samples", 100);
  Option<std::string> oOpt('o', "output",  "\tOutput file name","gains.csv");
  Option<unsigned>    sOpt('s', "step",    "\tPulser step size", 32);
  Option<unsigned>    tOpt('t', "sample",  "\tSample to use\t", 2);
  // FXS options 
  Option<int>         rOpt('r',"run",      "\tRun number\t", 0);
  Option<std::string> dOpt('d',"detector", "Detector\t", "FMD");
  Option<std::string> ROpt('R',"role",     "\tDAQ Role\t", "Calib");
  Option<std::string> POpt('P',"path",     "\tPath to FXS\t","localhost:/tmp");
  Option<std::string> DOpt('D',"database", "Database\t", 
			   "mysql://daq:daq@localhost/DATE_LOG");

  CommandLine cl("SOURCE");
  // General 
  cl.Add(hOpt);
  cl.Add(vOpt);
  // FMD 
  cl.Add(OOpt);
  cl.Add(mOpt);
  cl.Add(MOpt);
  cl.Add(cOpt);
  cl.Add(bOpt);
  // GainMaker
  cl.Add(nOpt);
  cl.Add(oOpt);
  cl.Add(sOpt);
  cl.Add(tOpt);
  // FXS 
  cl.Add(rOpt);
  cl.Add(DOpt);
  cl.Add(dOpt);
  cl.Add(ROpt);
  cl.Add(POpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
#ifdef HAVE_RCUXX
    Rcuxx::Rcu::PrintHelp(std::cout);
#endif
    return 0;
  }
  if (vOpt.IsSet()) {
    std::cout << "fmdgain version " << VERSION << std::endl;
    return 0;
  }

  std::string output = oOpt;
  std::string hw     = cOpt;
  const FmdDA::GainMaker::Sources_t& sources = cl.Remain();
  if (sources.size() < 1) {
    std::cerr << "No sources specified" << std::endl;
    return 0;
  }
  // FmdDA::GainMaker maker(sources, output.c_str(), 0, bOpt, true, true);
  FmdDA::GainMaker maker(sources, output.c_str(), 0, bOpt, true, true);
  maker.SetOverSample(OOpt);
  maker.SetRange(mOpt, MOpt);
  maker.SetNSamples(nOpt);
  maker.SetPulseStep(sOpt);
  maker.SetSample(tOpt);
  
  bool ret = maker.Run(hw);
  
  int retval = 1;
  if (ret) {
    std::string url  = DOpt;
    std::string det  = dOpt;
    std::string role = ROpt;
    std::string path = POpt;
   
#ifdef HAVE_DAQDA_H
    retval = daqDA_FES_storeFile(output.c_str(), "gains");
#else
    FmdDA::FXS fxs(url, rOpt, det, role, path);
    retval = fxs.CopyFile(output, "gains");
#endif
  }
  if (retval != 0) std::cerr << "Gain Maker failed" << std::endl;

  return retval;
}

//
// EOF
//
    
	
	
	
	 

