//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Gains.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:20:14 2006
    @brief   
    @ingroup fmdda_gains
*/
#include "Gains.h"
#include <rcugsl/Histogram.h>
#include <rcugsl/UnbinnedData.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cassert>

namespace FmdDA 
{
  namespace Gains
  {    
    //________________________________________________________________
    Fmd::Elem_t* Fmd::GetOrAdd(unsigned int id) 
    {
      if (!GetOrResize(id)) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Fmd::WriteOut() 
    {
      // if (fSummary) fSummary->Write();
    }
    //________________________________________________________________
    void Fmd::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Fmd::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Fmd::Fill(unsigned int ddl, 
		   unsigned int board,   unsigned int chip, 
		   unsigned int channel, unsigned int strip, 
		   unsigned int pulse,   unsigned int adc) 
    {
      Gains::Rcu* r = GetOrAdd(ddl);
      if (!r) return;
      r->Fill(board, chip, channel, strip, pulse, adc);
    }
    //==================================================================
    const char* Rcu::GetName() const 
    {
      if (fName.empty()) {
	std::stringstream s;
	s << "rcu_" << std::setw(1) << fId;
	fName = s.str();
      }
      return fName.c_str();
    }
    //________________________________________________________________
    Rcu::Elem_t* Rcu::GetOrAdd(unsigned int id) 
    {
      if (!GetOrResize(id)) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Rcu::WriteOut() 
    {
      // if (fSummary) fSummary->Write();
    }
    //________________________________________________________________
    void Rcu::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Rcu::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Rcu::Fill(unsigned int board,   unsigned int chip, 
		   unsigned int channel, unsigned int strip, 
		   unsigned int pulse,   unsigned int adc) 
    {
      Gains::Board* b = GetOrAdd(board);
      if (!b) return;
      b->Fill(chip, channel, strip, pulse, adc);
    }
    
    //==================================================================
    const char* Board::GetName() const 
    {
      if (fName.empty()) {
	std::stringstream s;
	s << "board_" << std::setw(2) << std::setfill('0') << fId;
	fName = s.str();
      }
      return fName.c_str();
    }
    //________________________________________________________________
    Board::Elem_t* Board::GetOrAdd(unsigned int id) 
    {
      if (!GetOrResize(id)) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Board::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Board::CountElem(Elem_t* e) const { return e->Count(); }
    
    //________________________________________________________________
    void Board::Fill(unsigned int chip,  unsigned int channel, 
		     unsigned int strip, unsigned int pulse, 
		     unsigned int adc) 
    {
      Gains::Chip* c = GetOrAdd(chip);
      if (!c) return;
      c->Fill(channel, strip, pulse, adc);
    }
  
    //==================================================================
    const char* Chip::GetName() const 
    {
      if (fName.empty()) {
	std::stringstream s;
	s << "chip_" << std::setw(1) << std::setfill('0') << fId;
	fName = s.str();
      }
      return fName.c_str();
    }
    //________________________________________________________________
    Chip::Elem_t* Chip::GetOrAdd(unsigned int id) 
    {
      if (!GetOrResize(id)) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Chip::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Chip::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Chip::Fill(unsigned int channel, unsigned int strip, 
		    unsigned int pulse,   unsigned int adc) 
    {
      Gains::Chan* c = GetOrAdd(channel);
      if (!c) return;
      c->Fill(strip, pulse, adc);
    }

    //==================================================================
    const char* Chan::GetName() const 
    {
      if (fName.empty()) {
	std::stringstream s;
	s << "channel_" << std::setw(2) << std::setfill('0') << fId;
	fName = s.str();
      }
      return fName.c_str();
    }
    //________________________________________________________________
    Chan::Elem_t* Chan::GetOrAdd(unsigned int id) 
    {
      if (!GetOrResize(id)) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Chan::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Chan::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Chan::Fill(unsigned int strip, 
		    unsigned int pulse,   unsigned int adc) 
    {
      Gains::Strip* s = GetOrAdd(strip);
      if (!s) return;
      s->Fill(pulse, adc);
    }


    //==================================================================
    const char* Strip::GetName() const 
    {
      if (fName.empty()) {
	std::stringstream s;
	s << "strip_" << std::setw(3) << std::setfill('0') << fId;
	fName = s.str();
      }
      return fName.c_str();
    }
    //________________________________________________________________
    size_t Strip::Count() const
    {
      size_t ret = fCont.size();
      return ret;
    }

    //________________________________________________________________
    RcuGsl::Histogram* Strip::GetOrAdd(unsigned int pulse) 
    {
      if (!GetOrResize(pulse)) {
	RcuGsl::Histogram* hist = new RcuGsl::Histogram(1024, -.5, 1023.5);
	fCont[pulse] = hist;
      }
      return fCont[pulse];
    }
    
    //________________________________________________________________
    void Strip::Fill(unsigned int s, unsigned int adc) 
    {
      RcuGsl::Histogram* h = GetOrAdd(s);
      // std::cout << h->GetName() << " fill at " << adc << std::endl;
      // h->AddBinContent(adc+1);
      // h->SetEntries(fCont[s]->GetEntries()+1);
      h->Fill(adc+1);
    }
    //__________________________________________________________________
    void Strip::WriteOut() 
    {
    }

    //__________________________________________________________________
    RcuGsl::UnbinnedData* 
    Strip::MakeData()
    {
      RcuGsl::UnbinnedData* ret = 0;
      if (fCont.size() <= 0) {
	std::cerr << "No histograms in this strip " << GetName() << std::endl;
	return ret;
      }

      size_t j = 0;
      size_t k = 0;
      ret = new RcuGsl::UnbinnedData(fCont.size(), true);
      for (Iter_t i = Begin(); i != End(); ++i, ++k) {
	RcuGsl::Histogram* hist  = *i; // i->second;
	unsigned int pulse = k; // i->first;
	if (!hist) continue;
	double mean = hist->Mean();
	double rms  = hist->Sigma();
	// Exclude outliers 
	int    lbin = hist->FindBin(std::max(0.,mean-2*rms));
	int    hbin = hist->FindBin(std::min(1023.,mean+2*rms));
	for (int k = 0; k < lbin; k++) 
	  hist->Fill(k, -hist->BinContent(k));
	for (unsigned int k = hbin+ 1 ; k < hist->Bins(); k++) 
	  hist->Fill(k, -hist->BinContent(k));
	mean = hist->Mean();
	rms  = hist->Sigma();
	ret->Set(j, pulse, mean, 0, rms);
	j++;
      }
      return ret;
    }

    //__________________________________________________________________
    void Strip::WriteElem(Elem_t* e) 
    {
      if (!e) return;
      // e->Write();
    }
    
    //__________________________________________________________________
    void
    Strip::Reset() 
    {
      Base_t::Reset();
    }
    //__________________________________________________________________
    void
    Strip::Clear() 
    {
      Base_t::Clear();
    }
  }
}
//
// EOF
//
