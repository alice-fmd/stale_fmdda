//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Spectra.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:20:14 2006
    @brief   
    @ingroup fmdda_spectra
*/
#include "fmdda/Spectra.h"
#include "fmdda/DebugGuard.h"
#include <rcugsl/Histogram.h>
#include <rcugsl/Function.h>
#include <rcugsl/UnbinnedData.h>
#include <iostream>
#include <iomanip>
#include <cassert>

namespace FmdDA 
{
  namespace Spectra
  {    
    //==================================================================
    Fmd::Fmd() 
      : Base_t(0,3)
    {
      // MakeSummary(1024, -.5, 1023.5);
      DGUARD("Top-level cache");
    }
    //________________________________________________________________
    Fmd::Elem_t* Fmd::GetOrAdd(unsigned int id) 
    {
      if (!GetOrResize(id)) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Fmd::WriteOut() 
    {
    }
    //________________________________________________________________
    void Fmd::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Fmd::CountElem(Elem_t* e) const { return e->Count(); }
    //________________________________________________________________
    void Fmd::PreallocSub(Elem_t* e) { return e->Prealloc(); }
    
    //________________________________________________________________
    void Fmd::Fill(unsigned int ddl, 
		   unsigned int board,   unsigned int chip, 
		   unsigned int channel, unsigned int strip, 
		   unsigned int s,       unsigned int adc) 
    {
      if (fSummary) fSummary->Fill(adc);
      Spectra::Rcu* r = GetOrAdd(ddl);
      if (!r) return;
      r->Fill(board, chip, channel, strip, s, adc);
    }

    //==================================================================
    Rcu::Rcu(unsigned int id, Fmd& fmd) 
      : Base_t(id,17), fFmd(&fmd)
    {
      DGUARD("Sub-detector cache for 0x%04x", id);
      // MakeSummary(1024, -.5, 1023.5);
    }
    //________________________________________________________________
    const char* Rcu::GetName() const 
    {
      if (fName.empty()) {
	std::stringstream s;
	s << "rcu_" << std::setw(1) << fId;
	fName = s.str();
      }
      return fName.c_str();
    }
    //________________________________________________________________
    Rcu::Elem_t* Rcu::GetOrAdd(unsigned int id) 
    {
      if (!GetOrResize(id)) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Rcu::WriteOut() 
    {
    }
    //________________________________________________________________
    void Rcu::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Rcu::CountElem(Elem_t* e) const { return e->Count(); }
    //________________________________________________________________
    void Rcu::PreallocSub(Elem_t* e) { return e->Prealloc(); }

    //________________________________________________________________
    void Rcu::Fill(unsigned int board,   unsigned int chip, 
		   unsigned int channel, unsigned int strip, 
		   unsigned int s,       unsigned int adc) 
    {
      if (fSummary) fSummary->Fill(adc);
      Spectra::Board* b = GetOrAdd(board);
      if (!b) return;
      b->Fill(chip, channel, strip, s, adc);
    }

    //==================================================================
    Board::Board(unsigned int id, Rcu& rcu) 
      : Base_t(id,3), 
	fRcu(&rcu)
    {
      DGUARD("Half-ring cache for 0x%04x/0x%02x", rcu.Id(), id);
      // MakeSummary(1024, -.5, -1023.5);
    }
    //________________________________________________________________
    const char* Board::GetName() const 
    {
      if (fName.empty()) {
	std::stringstream s;
	s << "board_" << std::setw(2) << std::setfill('0') << fId;
	fName = s.str();
      }
      return fName.c_str();
    }
    //________________________________________________________________
    Board::Elem_t* Board::GetOrAdd(unsigned int id) 
    {
      if (!GetOrResize(id)) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Board::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Board::CountElem(Elem_t* e) const { return e->Count(); }
    //________________________________________________________________
    void Board::PreallocSub(Elem_t* e) { return e->Prealloc(); }
    
    //________________________________________________________________
    void Board::Fill(unsigned int chip,  unsigned int channel, 
		     unsigned int strip, unsigned int s,     
		     unsigned int adc) 
    {
      if (fSummary) fSummary->Fill(adc);
      Spectra::Chip* c = GetOrAdd(chip);
      if (!c) return;
      c->Fill(channel, strip, s, adc);
    }
    //==================================================================
    Chip::Chip(unsigned int id, Board& board) 
      : Base_t(id,16), 
	fBoard(&board)
    {
      DGUARD("Altro cache for 0x%04x/0x%02x/0x%1x", 
	     board.RcuNo(), board.Id(), id);
      // MakeSummary(1024, -.5, -1023.5);
    }
    //________________________________________________________________
    const char* Chip::GetName() const 
    {
      if (fName.empty()) {
	std::stringstream s;
	s << "chip_" << std::setw(1) << std::setfill('0') << fId;
	fName = s.str();
      }
      return fName.c_str();
    }
    //________________________________________________________________
    Chip::Elem_t* Chip::GetOrAdd(unsigned int id) 
    {
      if (!GetOrResize(id)) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Chip::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Chip::CountElem(Elem_t* e) const { return e->Count(); }
    //________________________________________________________________
    void Chip::PreallocSub(Elem_t* e) { return e->Prealloc(); }

    //________________________________________________________________
    void Chip::Fill(unsigned int channel, unsigned int strip, 
		    unsigned int s,       unsigned int adc) 
    {
      if (fSummary) fSummary->Fill(adc);
      Spectra::Chan* c = GetOrAdd(channel);
      if (!c) return;
      c->Fill(strip, s, adc);
    }

    //==================================================================
    Chan::Chan(unsigned int id, Chip& chip) 
      : Base_t(id,127), 
	fChip(&chip)
    {
      DGUARD("Channel cache for 0x%04x/0x%02x/0x%1x/0x%02x", 
	     chip.RcuNo(), chip.BoardNo(), chip.Id(), id);
      // MakeSummary(1024, -.5, -1023.5);
    }

    //________________________________________________________________
    const char* Chan::GetName() const 
    {
      if (fName.empty()) {
	std::stringstream s;
	s << "channel_" << std::setw(2) << std::setfill('0') << fId;
	fName = s.str();
      }
      return fName.c_str();
    }
    //________________________________________________________________
    Chan::Elem_t* Chan::GetOrAdd(unsigned int id) 
    {
      if (!GetOrResize(id)) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Chan::WriteElem(Elem_t* e) 
    { 
      if (!e) return;
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Chan::CountElem(Elem_t* e) const { return e->Count(); }
    //________________________________________________________________
    void Chan::PreallocSub(Elem_t* e) { return e->Prealloc(); }

    //________________________________________________________________
    void Chan::Fill(unsigned int strip, 
		    unsigned int s,       unsigned int adc) 
    {
      if (fSummary) fSummary->Fill(adc);
      Spectra::Strip* str = GetOrAdd(strip);
      if (!str) return;
      str->Fill(s, adc);
    }

    //==================================================================
    Strip::Strip(unsigned int id, Chan& chan) 
      : Base_t(id,4), 
	fChan(&chan)
    {
      DGUARD("Strip cache for 0x%04x/0x%02x/0x%1x/0x%02x [%3d]", 
	     chan.RcuNo(), chan.BoardNo(), chan.ChipNo(), chan.Id(), id);
      // MakeSummary(1024, -.5, -1023.5);
    }
    //________________________________________________________________
    const char* Strip::GetName() const 
    {
      if (fName.empty()) {
	std::stringstream s;
	s << "strip_" << std::setw(3) << std::setfill('0') << fId;
	fName = s.str();
      }
      return fName.c_str();
    }
    //________________________________________________________________
    size_t Strip::Count() const
    {
      size_t ret = fCont.size();
      return ret;
    }

    //________________________________________________________________
    RcuGsl::Histogram* Strip::GetOrAdd(unsigned int s) 
    {
      if (!GetOrResize(s)) 
	fCont[s] = new RcuGsl::Histogram(1024, -.5, 1023.5);
      return fCont[s];
    }
    
    //________________________________________________________________
    void Strip::Fill(unsigned int s, unsigned int adc) 
    {
      if (fSummary) fSummary->Fill(adc);
      RcuGsl::Histogram* h = GetOrAdd(s);
      // std::cout << h->GetName() << " fill at " << adc << std::endl;
      h->Fill(adc+1);
    }
    //__________________________________________________________________
    void Strip::WriteOut() 
    {
      // if (fSummary) fSummary->Write();
    }

    //__________________________________________________________________
    void Strip::WriteElem(Elem_t* e) 
    {
      if (!e) return;
      // e->Write();
    }
    
    //__________________________________________________________________
    void
    Strip::Reset() 
    {
      Base_t::Reset();
      if (fSummary) fSummary->Reset();
    }
    //__________________________________________________________________
    void
    Strip::Clear() 
    {
      Base_t::Clear();
      if (fSummary) {
	delete fSummary;
	fSummary = 0;
      }
    }
  }
}
//
// EOF
//
