// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    GainMaker.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Class to find pulser gain. 
    @ingroup fmdda_data    
*/
#ifndef FMDDA_GAINMAKER
#define FMDDA_GAINMAKER
#ifndef __CCTYPE__
# include <cctype>
#endif
#ifndef FMDDA_GAINS
# include <fmdda/Gains.h>
#endif
#ifndef FMDDA_Maker
# include <fmdda/Maker.h>
#endif

// Forward decls 
namespace RcuGsl
{
  class Fitter;
  class Function;
}

namespace FmdDA 
{
  /** @defgroup gains Pulser Gain extraction 
      @ingroup fmdda */

  /** @class GainMaker
      @brief Class to find pulser gain
      @ingroup gains */
  class GainMaker : public Maker
  {
  public:
    /** list of sources */ 
    typedef Maker::Sources_t Sources_t;

    /** Start of job.
	@param src   Input source
	@param out   Output file
	@param skip  # of evenns to skip
	@param all   Analyse all events 
	@param wait  Wait for data */
    GainMaker(const Sources_t&  src, 
	      const char*       out, 
	      unsigned int      skip=0, 
	      unsigned int      baseDDL=0x4e2a, 
	      bool              all=true, 
	      bool              wait=true);

    /** Called when ever we get a new channel from reader.
	@param ddl     DDL number
	@param board   Board number 
	@param chip    Chip number 
	@param channel Channel number 
	@param last    Last ADC word 
	@return @c true, means continue with this channel */
    virtual bool GotChannel(uint32_t ddl, 
			    uint32_t board, 
			    uint32_t chip, 
			    uint32_t channel, 
			    uint32_t last);
    /** Called when ever we get a new channel from reader.
	@param c      Channel object
	@param hasAll @c true if @a c has all information.  
	@return @c true, means continue with this channel */
    virtual bool GotChannel(FmdDA::Channel& c, bool hasAll);
    /** Called when ever we get a new ADC value for the current
	channel. 
	@param t   Time bin
	@param adc ADC value 
	@return  @c true, means continue with this channel */
    virtual bool GotData(uint10_t t, uint10_t adc);

    /** Set the sample to use 
	@param t     Sample to use (@f$ 0\leq
	t<\mbox{oversample~rate}@f$)*/
    void SetSample(uint32_t t=2) { fSample = t; }
    /** Set the fit function to use 
	@param f Fit function 
	@param par  Parameter that is the gain */
    void SetFitFunction(RcuGsl::Function* f, unsigned par) 
    { 
      fFunction = f; 
      fPar      = par; 
    }
  protected:
    /** At end of job */
    virtual bool Start();
    /** At end of job */
    virtual bool Event();
    /** At end of job */
    virtual bool End();
    /** Process a final spectra 
	@param spectra Spectra to process 
	@param ddl     DDL  #
	@param board   Board #
	@param chip    Chip (ALTRO) # 
	@param chan    Channel (VA1) # 
	@param strip   Strip #  */
    virtual bool ProcessGraph(RcuGsl::UnbinnedData* spectra, 
			      uint32_t ddl,
			      uint32_t board, 
			      uint32_t chip, 
			      uint32_t chan, 
			      uint32_t strip);

    /** Top-level of cache */
    Gains::Fmd  fTop;
    /** Current Rcu */
    Gains::Rcu*  fCurRcu;
    /** Current channel */ 
    Gains::Board* fCurBoard;
    /** Current channel */ 
    Gains::Chip* fCurChip;
    /** Current channel */ 
    Gains::Chan* fCurChan;
    /** Current strip */ 
    Gains::Strip*  fCurStrip;
    /** Fit function */
    RcuGsl::Function*      fFunction;
    /** The fitter object */
    RcuGsl::Fitter*	   fFitter;
    /** parameter that is the gain */
    unsigned int           fPar;
    /** Number of events/pulse/strip */
    unsigned int           fNSamples;
    /** pulse height step size */
    unsigned int           fPulseStep;
    /** Sample to use */
    unsigned int           fSample;
    /** Current strip no */
    std::vector<uint32_t> fCurrentStrip;
    /** Current pulser value */
    std::vector<uint32_t> fCurrentPulse;
    /** Current event in iteration */
    std::vector<int> fCurrentSample;
  };
}
#endif
//
// EOF
//

