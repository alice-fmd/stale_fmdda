// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    GainMaker.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Class to find pulser gain. 
    @ingroup fmdda_data    
*/
#include "config.h"
#include "GainMaker.h"
#include "Channel.h"
#include <rcugsl/LinearFitter.h>
#include <rcugsl/Polynomial.h>
#include <rcugsl/UnbinnedData.h>
#include <iostream>
#include <fstream>

struct StraightLine : public RcuGsl::Polynomial
{
  StraightLine(double inter=0, double slope=1) 
    : Polynomial(0., 0.)
  {
    Set(inter, slope);
  }
  void Set(double inter, double slope) 
  {
    fP[0] = inter;
    fP[1] = slope;
  }
  void Reset() { RcuGsl::Polynomial::Reset(); Set(0, 1); }
};


//____________________________________________________________________
FmdDA::GainMaker::GainMaker(const Sources_t& src, 
			    const char*      out, 
			    unsigned int     skip, 
			    unsigned int     baseDDL, 
			    bool             all, 
			    bool             wait)
  : Maker(src, out, -1, 0, baseDDL, all, wait), 
    fCurRcu(0),
    fCurBoard(0), 
    fCurChip(0), 
    fCurChan(0), 
    fCurStrip(0),
    fFunction(0),
    fFitter(0), 
    fPar(0),
    fSample(0),
    fCurrentStrip(fFirst.size()), 
    fCurrentPulse(fStep.size()), 
    fCurrentSample(fIter.size())
{
  std::fill(fCurrentStrip.begin(),fCurrentStrip.end(), 0);
  std::fill(fCurrentPulse.begin(),fCurrentPulse.end(), 0);
  std::fill(fCurrentSample.begin(),fCurrentSample.end(), 0);
  SetSample();
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  fFitter   = new RcuGsl::LinearFitter;
  fFunction = new StraightLine;
  fPar      = 1;
  if (fOutput.empty()) fOutput = "gains.csv";
}

//____________________________________________________________________
bool
FmdDA::GainMaker::GotChannel(uint32_t ddl, 
			     uint32_t board, 
			     uint32_t chip, 
			     uint32_t chan, 
			     uint32_t last)
{
  if (fCurrentStrip[Address2Index(ddl,board)] > 
      fLast[Address2Index(ddl,board)]) return false;
  bool newRcu   = false;
  bool newBoard = false;
  bool newChip  = false;
  bool newChan  = false;

  unsigned int rddl = ddl-fBaseDDL;
  // We only move the pointers when we really need to
  if (!fCurRcu || fCurRcu->Id()!= rddl) {
    newRcu  = true; // Need new chip
    fCurRcu = fTop.GetOrAdd(rddl);
  }
  if (newRcu || !fCurBoard || fCurBoard->Id()!= board) {
    newBoard  = true; // Need new chip
    fCurBoard = fCurRcu->GetOrAdd(board);
  }
  if (newBoard || !fCurChip || fCurChip->Id() != chip)  { 
    newChip  = true; // Need new channel 
    fCurChip = fCurBoard->GetOrAdd(chip);
  }
  if (newChip || !fCurChan || fCurChan->Id() != chan)  { 
    newChan  = true; // Need new strip
    fCurChan = fCurChip->GetOrAdd(chan);
  }
  if (newChan || !fCurStrip || fCurStrip->Id() != fCurrentStrip[board]) 
    fCurStrip = fCurChan->GetOrAdd(fCurrentStrip[board]);

  return true;
}
  
//____________________________________________________________________
bool
FmdDA::GainMaker::GotChannel(FmdDA::Channel& c, bool valid)
{
  uint32_t  ddlno   = c.DDL();
  uint32_t  boardno = c.Board();
  uint32_t  idx     =  Address2Index(ddlno,boardno);
  if (fCurrentStrip[idx] > fLast[idx]) return false;
  Gains::Rcu*   rcu   = fTop.GetOrAdd(c.DDL());
  Gains::Board* board = rcu->GetOrAdd(c.Board());
  Gains::Chip*  chip  = board->GetOrAdd(c.Chip());
  Gains::Chan*  chan  = chip->GetOrAdd(c.ChanNo());
  fCurStrip           = chan->GetOrAdd(fCurrentStrip[idx]);

  if (!valid) return true;
  unsigned t   = fOffset + fExtraOffset + fSample;
  unsigned adc = c.fData[t];
  fCurStrip->Fill(fCurrentPulse[idx], adc);
  
  return false; // Do not continue 
}

//____________________________________________________________________
bool
FmdDA::GainMaker::GotData(uint10_t t, uint10_t adc) 
{
  // Check that we got the current channel 
  if (!fCurStrip) { 
    std::cout << "No current object" << std::endl;
    return false;
  }
  unsigned match = fOffset + fExtraOffset + fSample;
  // As we read raw data backward, we need to reverse the comparison
  if (t > match) return true;
  if (t < match) return false;
  // OK, we're were we want to be. 
  uint32_t ddlno   = fCurRcu->Id();
  uint32_t boardno = fCurBoard->Id();
  uint32_t idx     = Address2Index(ddlno, boardno);
  fCurStrip->Fill(fCurrentPulse[idx], adc);
  return false;
}

//____________________________________________________________________
bool
FmdDA::GainMaker::Start()
{
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  // Check some settings 
  bool         ret   = true;
  
  std::cout << " DDL    | Board # | Iter | Step | Over | Range\n"
	    << "--------+---------+------+------+------+-----------" 
	    << std::endl;
  for (size_t i = 0; i < fIter.size(); i++) {
    uint32_t ddl = i / fMaxBoard;
    std::cout << " 0x" << std::setfill('0') << std::hex 
	      << std::setw(4) << ddl << " | " 
	      << std::setw(2) << (i%fMaxBoard) << std::dec << std::setfill(' ') 
	      << " |" << std::flush;
    std::cout << std::setw(5) << fIter[i] << " |";
    if (fIter[i] <= 0) {
      std::cerr << "Invalid number of events/pulse/strip: " 
		<< fIter[i] << " for board " << i << std::endl;
      ret = false;
    }

    std::cout << std::setw(5) << fStep[i] << " |";
    if (fStep[i] <= 0) {
      std::cerr << "Invalid pulse height step: " << fStep[i] 
		<< " for board " << fStep[i] << std::endl;
      ret = false;
    }

    std::cout << std::setw(5) << fOver[i] << " |";
    if (fSample >= fOver[i]) {
      std::cerr << "Invalid sample number: " << fSample 
		<< "(>= " << fOver[i] << ") for board " << i << std::endl;
      ret = false;
    }

    std::cout << std::setw(4) << fFirst[i] << " -" << std::setw(4) << fLast[i];
    if (fFirst[i] > fLast[i] || fFirst[i] > 127 || fLast[i] > 127) {
      std::cerr << "Invalid strip range: [" << fFirst[i] << "," 
		<< fLast[i] << "] for board " << i << std::endl;
      ret = false;
    }
    std::cout << std::endl;
    if (!ret) return ret;
  }
  
  // Reset some counters 
  int total = 0;
  for (size_t i = 0; i < fFirst.size(); i++) {
    fCurrentStrip[i]  = fFirst[i];
    int t = fIter[i] * 255 / fStep[i] * (fLast[i]-fFirst[i]+1);
    total = std::max(t,total);
  }
  std::fill(fCurrentPulse.begin(), fCurrentPulse.end(), 0);
  std::fill(fCurrentSample.begin(), fCurrentSample.end(), 0);
  fMeter.Reset(total);

  fCurRcu   = 0;
  fCurBoard = 0;
  fCurChip  = 0;
  fCurChan  = 0;
  fCurStrip = 0;
  
  return ret;
}

//____________________________________________________________________
bool
FmdDA::GainMaker::Event()
{
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  bool cont = false;
  for (size_t i = 0; i < fCurrentSample.size(); i++) {
    fCurrentSample[i]++;
    if (fCurrentSample[i] >= int(fIter[i])) {
      fCurrentSample[i] =  0;
      fCurrentPulse[i]  += fStep[i];
      if (fCurrentPulse[i] >= 256) {
	fCurrentPulse[i] = 0;
	fCurrentStrip[i]++;
      }
      // If there's still more to be done for this board, continue;
      if (fCurrentStrip[i] <= fLast[i]) cont = true;
    }
    else cont = true;
  }
  fMeter.Step();
  return cont;
}

//____________________________________________________________________
bool
FmdDA::GainMaker::End()
{
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  typedef Gains::Fmd::Iter_t    rcuIter;
  typedef Gains::Rcu::Iter_t    boardIter;
  typedef Gains::Board::Iter_t  chipIter;
  typedef Gains::Chip::Iter_t   channelIter;
  typedef Gains::Chan::Iter_t   stripIter;
  typedef Gains::Strip::Iter_t  histIter;

  // Open output file
  std::ofstream out(fOutput.c_str());
  if (!out || out.bad()) {
    std::cerr << "Failed to open output file \"" << fOutput 
	      << "\"" << std::endl;
    return false;
  }
  // Write header. 
  out << "# Gains\n" 
      << "# Rcu,Board,Chip,Channel,Strip,Gain,Error,Chi2/NDF" << std::endl;
  fCsv = &out;
  
  // Loop over accumalted data 
  bool ret = true;
  for (rcuIter ir = fTop.Begin(); ir != fTop.End(); ++ir) {
    Gains::Rcu* rcu = *ir; // ir->second;
    if (!rcu) continue;
    for (boardIter ib = rcu->Begin(); ib != rcu->End(); ++ib) {
      Gains::Board* board = *ib; // ib->second;
      if (!board) continue;
      for (chipIter ia = board->Begin(); ia != board->End(); ++ia){
	Gains::Chip* chip = *ia; // ia->second;
	if (!chip) continue;
	for (channelIter ic = chip->Begin(); ic != chip->End(); ++ic){
	  Gains::Chan* chan = *ic; // ic->second;
	  if (!chan) continue;
	  for (stripIter is = chan->Begin(); is != chan->End(); ++is){
	    Gains::Strip*         strip = *is; // is->second;
	    if (!strip) continue;
	    // Create `graph' of (pulse,adc) pairs 
	    RcuGsl::UnbinnedData* graph = strip->MakeData();
	    // Process the (pulse,adc) pairs 
	    if (!ProcessGraph(graph, rcu->Id(), board->Id(), chip->Id(), 
			      chan->Id(),strip->Id())) {
	      ret = false;
	      break;
	    }
	  } // for Strip ...
	  if (!ret) break;
	} // for Chan ...
	if (!ret) break;
      }  // for Chip ...
      if (!ret) break;
    } // for Board ...
    if (!ret) break;
  } // for Rcu
  if (!ret) out << "# Failed!" << std::endl;
  out << "# EOF" << std::endl;
  out.close();
  fCsv = 0;
  return ret;
}

//____________________________________________________________________
bool
FmdDA::GainMaker::ProcessGraph(RcuGsl::UnbinnedData* spectra, 
			       uint32_t ddl,
			       uint32_t board, 
			       uint32_t chip, 
			       uint32_t chan, 
			       uint32_t strip)
{
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  if (!fFitter) return false;
  fFunction->Reset();
  fFitter->Set(*fFunction, *spectra);
  // std::cout << "Fitting for " << board << "/" << chip << "/" << chan 
  //  << "/" << strip << " ... " << std::flush;
  try {
    do {
      fFitter->Step();
      // if (verb) f.Print(0x3);
    } while (!fFitter->TestDelta() && fFitter->Steps() < 300);
    float gain  = fFunction->Parameters()[fPar];
    float err   = fFunction->Errors()[fPar];
    float chi2  = fFitter->ChiSquare() / fFitter->DegreesOfFreedom();
    *fCsv << ddl   << "," 
	  << board << "," 
	  << chip  << "," 
	  << chan  << "," 
	  << strip << ","
	  << gain  << "," 
	  << err   << "," 
	  << chi2  << std::endl;
    // std::cout << gain << std::endl;
  } catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return false;
  }
  return true;
}

  


//____________________________________________________________________
//
// EOF
//
