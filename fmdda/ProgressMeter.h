// -*- mode: c++ -*-  
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:04:26 2006
    @brief   Base class for data acquisition - declaration
*/
#ifndef FMDDA_PROGRESSMETER_H
#define FMDDA_PROGRESSMETER_H
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif

namespace FmdDA 
{
  /** @class ProgressMeter fmdda/ProgressMeter.h <fmdda/ProgressMeter.h>
      @ingroup fmdda_env
   */
  class ProgressMeter
  {
  public:
    /** Constructor 
	@param n Number of iterations */
    ProgressMeter(int n=0) { Reset(n); }
    /** Step on iteration */
    void Step();
    /** Reset progress bar to new number of iterations 
	@param n New number of iterations */
    void Reset(int n) { fN = n; fCurrent = fLast = 0; }
    /** @return  The current count */
    unsigned long Current() const { return fCurrent; }
    /** Update the total number */
    void SetN(unsigned long n) { fN = n; }
    /** Get the total number */
    unsigned long N() const { return fN; }
  private:
    /** Number of iterations */
    int    fN;
    /** Current iteration */
    unsigned long fCurrent;
    /** Last percentage */ 
    unsigned int fLast;
  };  

  //____________________________________________________________________
  inline void
  ProgressMeter::Step()
  {
    fCurrent++;
    if (fLast >= 100) return;
    unsigned rel = unsigned(100 * (double(fCurrent) / fN + 0.0));
    if (rel == fLast) return;
    fLast = rel;

    std::cout << "Progress: " << std::setfill('0') << std::setw(2)
	      << rel << std::setfill(' ') << " (" 
	      << std::setw(6) << fCurrent << "/" << std::setw(6) 
	      << fN << ")" << std::endl;
  }
}

#endif
//
// EOF
//
