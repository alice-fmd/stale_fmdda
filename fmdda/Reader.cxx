//
<<<<<<< Reader.cxx
// $Id: Reader.cxx,v 1.4 2009-02-09 23:12:33 hehi Exp $
=======
// $Id: Reader.cxx,v 1.4 2009-02-09 23:12:33 hehi Exp $
>>>>>>> 1.3
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Base class for reading channel information.
*/
#include "config.h"
#include "Reader.h"
#include "Channel.h"
#include "AltroDecoder.h"
#include <iostream>
# include <readraw/Reader.h>


//____________________________________________________________________
FmdDA::Reader::~Reader() 
{
  if (fReader) delete fReader;
  if (fMask)   delete fMask;
}

//____________________________________________________________________
void
FmdDA::Reader::SetMask(bool all, bool block) 
{
  fMask = new ReadRaw::Mask;
  fMask->AddToMask(ReadRaw::Mask::kPhysicsEvent, 
		   (all ? ReadRaw::Mask::kAll : ReadRaw::Mask::kSome));
  fReader->SetMask(*fMask); 
  fReader->SetWait(block);
  fIsEOD = false;
}

//____________________________________________________________________
bool 
FmdDA::Reader::SetInput(const std::vector<std::string>& src, 
			bool all, bool block)
{
  if (fReader) {
    std::cerr << "Cannot add more input to a RawReader" << std::endl;
    exit(1);
  }
  fReader = ReadRaw::Reader::Create(src, true);
  if (!fReader) { 
    std::cerr << "No raw reader created for";
    for (size_t i = 0; i < src.size(); i++) 
      std::cerr << (i == 0 ? " " : ", ") << src[i];
    std::cerr<< std::endl;
    return false;
  }
  SetMask(all, block);
  std::cout << "Total number of events: " << fReader->Events() << std::endl;
  return true;
}

//____________________________________________________________________
bool 
FmdDA::Reader::SetInput(const char* src, bool all, bool block) 
{
  if (fReader) {
    std::cerr << "Cannot add more input to a RawReader" << std::endl;
    exit(1);
  }
  fReader = ReadRaw::Reader::Create(src, true);
  if (!fReader) { 
    std::cerr << "No raw reader created for " << src << std::endl;
    return false;
  }
  fReader->SetVerbose(true);
  SetMask(all, block);
  return true;
}
    
//____________________________________________________________________
int 
FmdDA::Reader::GetNextEvent() 
{
 
  fIsEOD = false;
  if (!fReader) {
    std::cerr << "No input set" << std::endl;
    fIsEOD = true;
    return kEndOfData;
  }
  // Check that we're not at the end. 
  if (fReader->IsEOD()) {
    // std::cout << "At EOD" << std::endl;
    fIsEOD = true;
    return kEndOfData;
  }
      
  // Check that we haven't passed the requested number of events. 
  if (fMaxEvents > 0 && int(fCurrentEvent) >= fMaxEvents) {
    fIsEOD = true;	
    // std::cout << "At MAX" << std::endl;
    return kMaxReached;
  }
      
  // Read in the next event 
  ReadRaw::Event event;
  bool ret = fReader->GetNextEvent(event);
  if (!ret) return kNoData;
  
  // Check whether we should skip this event.
  if (fCurrentEvent < fSkipEvents) {
    // std::cout << "Skipi" << std::endl;
    fCurrentEvent++;
    return kSkip;
  }
      
  // Get the event number 
  fLastEvent = event.Head().Id().NumberInRun();
  
  // Now visit the event. 
 
  fProc.Visit(event);
  
  // Delete the event structure, increment the event counter and
  // return 
  // delete event;
  fCurrentEvent++;
  return kData;
}

//____________________________________________________________________
long 
FmdDA::Reader::GetNumberOfEvents() const 
{
  if (fMaxEvents < 0 && fReader) return fReader->Events();
  return fMaxEvents;
}

#if 0    
//____________________________________________________________________
void 
FmdDA::Reader::GotChannel(unsigned int board, 
			  unsigned int chip, 
			  unsigned int channel, 
			  unsigned int last) 
{
  FmdDA::RawVisitor::GotChannel(board, chip, channel, last);
  fSkipChannel = false;
  if (!fCurrent) return;
  fSkipChannel = !fProc.GotChannel(*fCurrent, false);
}

//____________________________________________________________________
void 
FmdDA::Reader::GotData(unsigned int t, unsigned int adc) 
{
  FmdDA::RawVisitor::GotData(t, adc);
  if (fSkipChannel) return;
  if (!fCurrent)    return;
  fSkipChannel = !fProc.GotData(t, adc);
};
#endif
//____________________________________________________________________
//
// EOF
//
