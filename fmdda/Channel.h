// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef FMDDA_CHANNEL_H
#define FMDDA_CHANNEL_H

/** @defgroup fmdda FMD DAQ Detector Algorithms 
 */
/** @namespace FmdDA RCU data structures for ROOT
    @ingroup fmdda
 */
namespace FmdDA 
{
  /** @defgroup fmdda_raw Decoding of raw data 
      @ingroup fmdda */

  /** @struct Header fmdda/Channel.h <fmdda/Channel.h>
      @brief Simple event header 
      @ingroup fmdda_raw */
  struct Header 
  {
    /** Event number */
    int fEventNo;
    /** Number of channels in the event */
    int fNChannels;
  };
  
  /** @class Channel fmdda/Channel.h <fmdda/Channel.h>
      @brief Channel data 
      @ingroup fmdda
  */
  class Channel
  {
  public:
    /** DDL number */ 
    unsigned short fDDL;
    /** Channel number */
    unsigned short fBoard;
    /** Channel number */
    unsigned short fChannel;
    /** Chip number */
    unsigned short fChip;
    /** Last 10bit word written */
    unsigned short fLast;
    /** First bin with data */
    unsigned short fM;
    /** Last bins with data */
    unsigned short fN;
    /** The data */
    unsigned short fData[1024];

    /** CTOR */
    Channel();
    /** CTOR 
	@param data Data to read from 
	@param size Size of @a data */
    Channel(const unsigned int* data, unsigned int size);
    /** DTOR */
    virtual ~Channel();

    /** Clear the channel information */
    void Clear();
    /** @return @c true if channel contains valid data */
    bool IsValid() const { return (fN > fM); }
    /** @return Board number  */
    unsigned int DDL() const { return fDDL; }
    /** @return Board number  */
    unsigned int Board() const { return fBoard; }
    /** @return Chip number  */
    unsigned int Chip() const { return fChip; }
    /** @return Channel number  */
    unsigned int ChanNo() const { return fChannel; }
    /** @return Least time bin */ 
    unsigned int MinT() const { return fM; }
    /** @return Largets time bin */ 
    unsigned int MaxT() const { return fN; }

    /** Print the channel data */
    void Print() const;
  };
}

#endif
//
// EOF
//
