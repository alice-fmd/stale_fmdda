//
// $Id: FXS.cxx,v 1.5 2009-02-09 23:12:33 hehi Exp $
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   File eXchange Server interface 
*/
#include "config.h"
#include "FXS.h"
#include <sys/stat.h>
#include <ctime>
#include <cerrno>
#include <cstdio>
#include <iostream>
#include <fstream>

namespace 
{
  //__________________________________________________________________
  /** @class InsertGuard 
      @brief A guard class to ensure that we leave the data base in a
      good state.  On construction, and entry is written to the data
      base.  If the user calls the member function Failure sometime
      between the contruction of an object of this class, and the
      destruction of the object, then, the database entry is removed. 

      @code 
      void Some::Scope() 
      { 
      InsertGuard guard(fServer, fRun, fDetector, fPath, size, 
      created, source, md5);
      if (!guard.IsSuccess()) return;

      ...
      if (!ok) guard.Failure(); // Indicate we should roll-back
	 
      // Guard object goes out of scope. If Failure was called, 
      // then the automatic destruction of the guard object
      // ensures that we roll-back to the previous state. 
      }
      @endcode 

      This is especially useful with exceptions. 
  */
  struct InsertGuard 
  {
    /** Construcor 
	@param server    Database server
	@param run       Run 
	@param detector  Detector code
	@param role      Role name
	@param path      Path of file on FXS
	@param fileId    File identifier 
	@param size      Size of file in bytes
	@param created   Date of creation (in seconds since epoc)
	@param md5       MD5 check sum of file */
    InsertGuard(RcuDb::Server*     server, 
	       const std::string& test,
		unsigned int      run,
	       const std::string& detector, 
	       const std::string& role, 
	       const std::string& fileId, 
	       unsigned long      size,
	       unsigned long      created, 
	       const std::string& path,
	       const std::string& md5);
    /** Destructor.  If Failure was called, this will remove the entry
	inserted by the constructor */
    ~InsertGuard();
    /** @return @c true if no error occured, or the user has called
	Failure  */
    bool IsSuccess() const { return fSuccess; }
    /** Indiate success */
    void Success() { fSuccess = true; }
    /** Indiate failure - causes a roll-back */
    void Failure() { fSuccess = false; }
  protected: 
    /** Database server */
    RcuDb::Server* fServer;
    /** File eXchange Server Test directory */ 
    std::string    fTest;
    /** Run number */
    unsigned int   fRun;
    /** Detector string */
    std::string    fDetector;
    /** File identifer */
    std::string    fFileId;
    /** Path of file on FXS */
    std::string    fPath;
    /** Current status */
    bool           fSuccess;
  };

  //====================================================================
  InsertGuard::InsertGuard(RcuDb::Server*     server, 
			   const std::string& test,
			   unsigned int       run,
			   const std::string& detector, 
			   const std::string& role, 
			   const std::string& fileId, 
			   unsigned long      size,
			   unsigned long      created, 
			   const std::string& path,
			   const std::string& md5)
    : fServer(server), 
      fTest(test),
      fRun(run),
      fDetector(detector), 
      fFileId(fileId), 
      fPath(path)
  {
    if (!fServer) {
      std::string outname = fTest + "/daqFES.db";
      std::ofstream out(outname.c_str(), std::ios_base::app);
      if (!out) return;
      out << "Run:'"         << fRun      << "' "
	  << "Detector:'"    << fDetector << "' "
	  << "fileId:'"      << fFileId   << "' " 
	  << "filePath:'"    << fPath     << "' "
	  << "fileSize:'"    << size      << "' "
	  << "timeCreated:'" << created   << "' "
	  << "daqSource:'"   << role      << "' "
	  << "MD5:'"         << md5       << "'"   
	  << std::endl;
      out.close();
      fSuccess = true;
      return;
    }
    RcuDb::Sql sql;
    sql << "INSERT INTO daqFES_files(" 
	<< "run,"
	<< "detector,"
	<< "fileId,"
	<< "filePath, "
	<< "size,"
	<< "time_created,"
	<< "DAQsource,"
	<< "fileCheckSum"
	<< ") VALUES ("
	<< "'" << fRun      << "', " 
	<< "'" << fDetector << "', " 
	<< "'" << fFileId   << "', " 
	<< "'" << fPath     << "', " 
	<< "'" << size      << "', " 
	<< "'" << created   << "', " 
	<< "'" << role      << "', " 
	<< "'" << md5       << "')";
    fSuccess = fServer->Exec(sql);
    if (!fSuccess) 
      std::cerr << "When inserting values:\n"
		<< "\t\"" << sql.Text() << "\"\n" 
		<< "\t-> " << fServer->ErrorString() << std::endl;
  }

  //____________________________________________________________________
  InsertGuard::~InsertGuard() 
  {
    if (fSuccess) return;
    std::cout << "Rolling back!" << std::endl;
    if (!fServer) {
      std::string outname = fTest + "/daqFES.db";
      std::ofstream out(outname.c_str(), std::ios_base::app);
      if (!out) return;
      out << "Remove entry "
	  << "Run:'"         << fRun      << "' "
	  << "Detector:'"    << fDetector << "' "
	  << "fileId:'"      << fFileId   << "' " 
	  << "filePath:'"    << fPath     << "'" << std::endl;
      out.close();
      return;
    }
    RcuDb::Sql sql;
    sql << "DELETE FROM daqFES_files WHERE " 
	<< "run='"      << fRun      << "' and "
	<< "detector='" << fDetector << "' and " 
	<< "fileId='"   << fFileId   << "' and " 
	<< "filePath='" << fPath     << "'";
    if (!fServer->Exec(sql)) 
      std::cerr << "When deleting values:\n"
		<< "\t\"" << sql.Text() << "\"\n" 
		<< "\t-> " << fServer->ErrorString() << std::endl;
    
  }
}


//====================================================================
FmdDA::FXS::FXS(const std::string& con, 
		int                run, 
		const std::string& detector, 
		const std::string& role, 
		const std::string& path) 
  : fInit(false), 
    fTest(false), 
    fServer(0), 
    fCon(con), 
    fRun(run), 
    fDetector(detector), 
    fRole(role), 
    fPath(path)
{
  if (getenv("DAQDA_TEST_DIR")) fTest = true;
  if (!fTest) return;
  fPath     = getenv("DAQDA_TEST_DIR");
  fRun      = 0;
  fRole     = "test";
  fDetector = "DAQ";
}

//____________________________________________________________________
FmdDA::FXS::~FXS()
{
  if (fServer && fServer->IsConnected()) delete fServer;
}

//____________________________________________________________________
int
FmdDA::FXS::Connect()
{
  RcuDb::Url url("");
  if (!fCon.empty()) url = fCon;
  else {
    url.SetScheme("mysql:");
    if (getenv("DATE_INFOLOGGER_MYSQL_USER")) 
      url.SetUser(getenv("DATE_INFOLOGGER_MYSQL_USER"));
    if (getenv("DATE_INFOLOGGER_MYSQL_PWD")) 
      url.SetPassword(getenv("DATE_INFOLOGGER_MYSQL_PWD"));
    if (getenv("DATE_INFOLOGGER_MYSQL_HOST")) 
      url.SetHost(getenv("DATE_INFOLOGGER_MYSQL_HOST"));
    if (getenv("DATE_INFOLOGGER_MYSQL_DB")) 
      url.SetPath(getenv("DATE_INFOLOGGER_MYSQL_DB"));
  }
  if (url.Host().empty() || url.Path().empty()) {
    std::cout << "Database parameters not (fully) specified" << std::endl;
    return 1;
  }

  fServer = RcuDb::Server::Connect(url.Raw());
  if (!fServer) {
    std::cerr << "Failed to connect to \"" << url.Raw() << "\"" << std::endl;
    return 2;
  }
  return 0;
}

//____________________________________________________________________
int
FmdDA::FXS::GetPath() 
{
  if (fPath.empty() && getenv("DATE_FES_PATH")) 
    fPath = getenv("DATE_FES_PATH");
  if (fPath.empty()) {
    std::cerr << "DATE_FES_PATH undefined" << std::endl;
    return 21;
  }
  return 0;
}

//____________________________________________________________________
int
FmdDA::FXS::GetRun() 
{
  if (fRun < 0 && getenv("DATE_RUN_NUMBER")) {
    std::stringstream s(getenv("DATE_RUN_NUMBER"));
    s >> fRun;
  }
  if (fRun < 0) {
    std::cerr << "DATE_RUN_NUMBER undefined" << std::endl;
    return 22;
  }
  return 0;
}

//____________________________________________________________________
int
FmdDA::FXS::GetRole() 
{
  if (fRole.empty() && getenv("DATE_ROLE_NAME"))
    fRole = getenv("DATE_ROLE_NAME");
  if (fRole.empty()) {
    std::cerr << "DATE_ROLE_NAME undefined" << std::endl;
    return 23;
  }
  return 0;
}

//____________________________________________________________________
int
FmdDA::FXS::GetDetector() 
{
  if (fDetector.empty() && getenv("DATE_DETECTOR_CODE")) 
    fDetector = getenv("DATE_DETECTOR_CODE");
  if (fDetector.empty()) {
    std::cerr << "DATE_DETECTOR_CODE undefined" << std::endl;
    return 24;
  }
  return 0;
}



//____________________________________________________________________
int
FmdDA::FXS::CopyFile(const std::string& source, const std::string& fileId) 
{
  int ret = 0;
  if (!fInit) {
    fInit = true;
    if (!fTest) {
      if ((ret = Connect()))     return ret;
      if ((ret = GetRun()))      return ret;
      if ((ret = GetDetector())) return ret;
      if ((ret = GetRole()))     return ret;
      if ((ret = GetPath()))     return ret;
    }
  }
  
  if (source.empty()) {
    std::cerr << "Missing source file" << std::endl;
    return 31;
  }
  if (fileId.empty()) {
    std::cerr << "Missing file id" << std::endl;
    return 32;
  }
  
  // Stat the file 
  struct stat buf;
  if (stat(source.c_str(), &buf)) {
    std::cerr << "Failed to stat \"" << source << "\": " 
	      << strerror(errno) << std::endl;
    return 4;
  }
  
  // Get the size of the file 
  unsigned long size = buf.st_size;
  
  // Get the current time
  unsigned long now = time(NULL);
  
  // Get the md5 checksum of the file. 
  std::string md5_str("md5sum ");
  md5_str += source;
  FILE* pipe = popen(md5_str.c_str(), "r");
  if (!pipe) {
    std::cerr << "Failed to execute \"" << md5_str << "\"" << std::endl;
    return false;
  }
  char md5_sum[33];
  if (!fgets(md5_sum, 33, pipe)) {
    std::cerr << "Failed to read MD5 sum from pipe" << std::endl;
    return false;
  }
  std::string md5(md5_sum);
  

  // Make the target file name 
  std::stringstream spath;
  spath << "run" << std::setfill('0') << std::setw(9) << fRun
	<< "_"   << fDetector 
	<< "_"   << fRole 
	<< "_"   << fileId;
  std::string path = spath.str();

  // Make a session guard - inserts entry in database
  InsertGuard guard(fServer, fPath, fRun, fDetector, fRole, 
		    fileId, size, now, path, md5); 
  if (!guard.IsSuccess()) {
    std::cerr << "Failed to insert entry into database";
    guard.Success();
    return 5;
  }
  
  // Now, we should do an scp to the FXS 
  std::string cp = (fTest ? "cp" : "scp");
  std::stringstream cp_cmd;
  cp_cmd << cp << " " << source << " " << fPath << "/" << path;
  std::string cp_str = cp_cmd.str();
  std::cout << cp_str << std::endl;
  ret = system(cp_str.c_str());


  // Check for error andd possibly indicate that we should role-back
  if (ret != 0) { 
    std::cerr << "Failed to copy file" << std::endl;
    guard.Failure();     
    return 6;
  }

  // If everything went well
  std::cout << source << " copied to " << (fTest ? "dummy " : "") 
	    << "DAQ File Exchange Server" << std::endl;
  return 0;
}

//
// EOF
//

