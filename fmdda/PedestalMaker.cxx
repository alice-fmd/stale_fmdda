// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Class to find pulser spectrum. 
    @ingroup fmdda_data    
*/
#include "config.h"
#include "PedestalMaker.h"
#include "Channel.h"
#include "DebugGuard.h"
#include <rcugsl/Gaus.h>
#include <rcugsl/NonLinearFitter.h>
#include <rcugsl/Histogram.h>
#include <iostream>
#include <fstream>

//____________________________________________________________________
/** Helper function to print histogram and fit via gnuplot 
    @param o    Output stream 
    @param g	Funcion 
    @param d	Data set 
    @param chi2 @f$\chi^2@f$ 
    @param ndf  Number degrees of freedom 
    @param print Print to file, not screen */
void
print_gnuplot(std::ostream& o, const RcuGsl::Function& g, 
	      const RcuGsl::DataSet& d, double chi2, int ndf, bool print)  
{
  g.ToScript(o, RcuGsl::kGnuplotScript);
  if (print) 
    o << "set terminal postscript eps enhanced;\n" 
      << "set output 'graph.eps';" << std::endl;
  else 
    o << "set terminal x11 enhanced;\n";
  o << "set label '{/Symbol c}^2/{/Times-Roman NDF}: %8.4f'," << chi2 
    << ",'/%3.0f'," << ndf << ",' = %8.4f'," << chi2/ndf 
    << " at graph 0.7,0.8;" << std::endl;
  for (size_t i = 0; i < g.Size(); i++) 
    o << "set label '" << std::setw(10) << g.names()[i] 
      << ": %8.4f {\\261} ',"
      << g.Parameters()[i] << ",'%8.4f'," << g.Errors()[i]
      << " at graph 0.7," << 0.75-.05*i << ";" << std::endl;
  o << "plot f(x) title 'fit', '-' ";
  if (d.HasErrors()) o << "using 1:2:4 with errorbars ";
  o << "title 'data'\n";
  d.ToScript(o, RcuGsl::kGnuplotScript);
  o << "\ne;" << std::endl;
}

//____________________________________________________________________
FmdDA::PedestalMaker::PedestalMaker(const Sources_t& src, 
				    const char*      out,
				    const char*      chdir,
				    int              n, 
				    unsigned int     skip, 
				    unsigned int     baseDDL, 
				    bool             all, 
				    bool             wait)
  : Maker(src, out, n, skip, baseDDL, all, wait), 
    fCurRcu(0),
    fCurBoard(0), 
    fCurChip(0),
    fCurChan(0),
    fFitter(0),
    fFunction(0),
    fChDir(chdir),
    fChOut(0)
{
  DGUARD("(src=%p,out=%s,chdir=%s,n=%d,skip=%d,base=%d,all=%s,wait=%s)",
	 &(src[0]), out, chdir, n, skip, baseDDL, (all ? "yes" : "no"),
	 (wait ? "yes" : "no"));
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  if (fOutput.empty()) fOutput = "peds.csv";
  if (fChDir.empty())  fChDir  = "./";
  // DMESG("Preallocating cache");
  // fTop.Prealloc();
}

//____________________________________________________________________
FmdDA::PedestalMaker::~PedestalMaker()
{
  if (fFitter) delete fFitter;
  if (fFunction) delete fFunction;
}

//____________________________________________________________________
void
FmdDA::PedestalMaker::SetFit(bool use)
{
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  if (use && !fFitter) {
    fFunction = new RcuGsl::Gaus(1, 0, 1);
    fFitter   = new RcuGsl::NonLinearFitter;
  }
  if (!use && fFitter) {
    delete fFitter;
    delete fFunction;
    fFitter   = 0;
    fFunction = 0;
  }
}

//____________________________________________________________________
bool
FmdDA::PedestalMaker::Start()
{
  fCurRcu   = 0;
  fCurBoard = 0;
  fCurChip  = 0;
  fCurChan  = 0;
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  return true;
}

//____________________________________________________________________
bool
FmdDA::PedestalMaker::GotChannel(uint32_t ddl, 
				 uint32_t board, 
				 uint32_t chip, 
				 uint32_t chan, 
				 uint32_t last)
{
  DGUARD("Got channel DDL=0x%04x Address=0x%02x/0x%01x/0x%02x Last=%d",
	 ddl, board, chip, chan, last);

  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  bool newRcu   = false;
  bool newBoard = false;
  bool newChip  = false;
  bool newChan  = false;
  
  // We only move the pointers when we really need to
  uint32_t rddl = ddl-fBaseDDL;
  if (!fCurRcu   || fCurRcu->Id() != rddl) { 
    DMESG("New RCU 0x%x (was 0x%x)", rddl, 
	  (!fCurRcu ? 0xFFFF : fCurRcu->Id()));
    newRcu = true;
    fCurRcu = fTop.GetOrAdd(rddl);
  }
  if (newRcu || !fCurBoard || fCurBoard->Id() != board) {
    DMESG("New Board 0x%x (was 0x%x)",board , 
	  (!fCurBoard ? 0xFFFF : fCurBoard->Id()));
    newBoard  = true;
    fCurBoard = fCurRcu->GetOrAdd(board);
  }
  if (newBoard || !fCurChip || fCurChip->Id() != chip)  { 
    DMESG("New Chip 0x%x (was 0x%x)",chip , 
	  (!fCurChip ? 0xFFFF : fCurChip->Id()));
    newChip  = true;
    fCurChip = fCurBoard->GetOrAdd(chip);
  }
  if (newChip || !fCurChan || fCurChan->Id() != chan)  { 
    DMESG("New Channel 0x%x (was 0x%x)",chan , 
	  (!fCurChan ? 0xFFFF : fCurChan->Id()));
    newChan  = true;
    fCurChan = fCurChip->GetOrAdd(chan);
  }
  return true;
}

//____________________________________________________________________
bool
FmdDA::PedestalMaker::GotChannel(FmdDA::Channel& c, bool)
{
  DGUARD("Got channel object DDL=0x%04x Address=0x%02x/0x%01x/0x%02x",
	 c.fDDL, c.fBoard, c.fChip, c.fChannel);
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  Spectra::Rcu*   rcu   = fTop.GetOrAdd(c.DDL());
  Spectra::Board* board = rcu->GetOrAdd(c.Board());
  Spectra::Chip*  chip  = board->GetOrAdd(c.Chip());
  fCurChan              = chip->GetOrAdd(c.ChanNo());
  return true;
}

//____________________________________________________________________
bool
FmdDA::PedestalMaker::GotData(uint10_t t, uint10_t adc) 
{
  // DGUARD("got ADC counts 0x%03x at t=%d", adc, t);
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  // Check that we got the current channel 
  if (!fCurChan) { 
    std::cout << "No current object" << std::endl;
    return false;
  }
  // Skip pre-stuff
  if (t < fOffset + fExtraOffset) return false;

  // Fill in proper histogram
  fTop.Fill(fCurChan->RcuNo(),
	    fCurChan->BoardNo(), 
	    fCurChan->ChipNo(), 
	    fCurChan->Id(), 
	    Timebin2Strip(fCurChan->RcuNo(),fCurChan->BoardNo(),t), 
	    Timebin2Sample(fCurChan->RcuNo(),fCurChan->BoardNo(),t), 
	    adc);
  return true;
}

//____________________________________________________________________
bool
FmdDA::PedestalMaker::End()
{
  DGUARD("Processing");
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  typedef Spectra::Fmd::Iter_t    rcuIter;
  typedef Spectra::Rcu::Iter_t    boardIter;
  typedef Spectra::Board::Iter_t  chipIter;
  typedef Spectra::Chip::Iter_t   channelIter;
  typedef Spectra::Chan::Iter_t   stripIter;
  typedef Spectra::Strip::Iter_t  histIter;

  // Open pedestal file 
  std::ofstream out(fOutput.c_str());
  if (!out || out.bad()) {
    std::cerr << "Failed to open output file \"" << fOutput << "\"" 
	      << std::endl;
    return false;
  }
  // Write header 
  out << "# Pedestals\n" 
      << "# Rcu, Board, Chip, Channel, Strip, Sample, TimeBin, "
      << "Pedestal, Noise, Mu, Sigma, Chi2/NDF" << std::endl;
  fCsv = &out;

  // Loop over spectra 
  bool ret = true;
  for (rcuIter ir = fTop.Begin(); ir != fTop.End(); ++ir) {
    Spectra::Rcu* rcu = *ir; // ir->second;
    if (!rcu) continue;
    for (boardIter ib = rcu->Begin(); ib != rcu->End(); ++ib) {
      Spectra::Board* board = *ib; // ib->second;
      if (!board) continue;
      // std::cout << "Board # " << board->Id() << std::endl;
      for (chipIter ia = board->Begin(); ia != board->End(); ++ia){
	Spectra::Chip* chip = *ia; // ia->second;
	if (!chip) continue;
	// std::cout << " Chip # " << chip->Id() << std::endl;
	for (channelIter ic = chip->Begin(); ic != chip->End(); ++ic){
	  Spectra::Chan* chan = *ic; // ic->second;
	  if (!chan) continue;
	  if (fChOut) {
	    fChOut->close();
	    fChOut = 0;
	  }
	  std::stringstream chs;
	  chs << fChDir << "/" << std::hex << std::setfill('0') 
	      << "0x" << std::setw(4) << rcu->Id()   << "_" 
	      << "0x" << std::setw(2) << board->Id() << "_"
	      << "0x" << std::setw(1) << chip->Id()  << "_"
	      << "0x" << std::setw(1) << chan->Id()  << ".pmem";
	  std::string chout = chs.str();
	  fChOut = new std::ofstream(chout.c_str());
	  if (!fChOut || !*fChOut) {
	    std::cerr << "Failed to open channel output file " 
		      << chout << std::endl;
	    return false;
	  }
	  // std::cout << "  Channel # " << chan->Id() << std::endl;
	  for (stripIter is = chan->Begin(); is != chan->End(); ++is){
	    Spectra::Strip* strip = *is; // is->second;
	    if (!strip) continue;
	    size_t n = 0;
	    for (histIter ih = strip->Begin(); ih != strip->End();++ih,++n){
	      // fMeter.Step();
	      RcuGsl::Histogram* spectrum = *ih; // ih->second;
	      if (!spectrum) continue;
	      ret = ProcessSpectrum(spectrum, 
				    rcu->Id(), board->Id(), chip->Id(), 
				    chan->Id(), strip->Id(), n);
	      if (!ret) break;
	    }
	    if (!ret) break;
	  } // for Strip ...
	  if (fChOut) { fChOut->close(); fChOut = 0; }
	  if (!ret) break;
	} // for Chan ...
	if (!ret) break;
      }  // for Chip ...
      if (!ret) break;
    } // for Board ...
    if (!ret) break;
  } // for Rcu
  if (!ret) out << "# Failed!" << std::endl;
  out << "# EOF" << std::endl;
  out.close();
  fCsv = 0;
  return ret;
}


//____________________________________________________________________
bool
FmdDA::PedestalMaker::ProcessSpectrum(RcuGsl::Histogram* spectra, 
				      uint32_t ddl,
				      uint32_t board, 
				      uint32_t chip, 
				      uint32_t chan, 
				      uint32_t strip, 
				      uint32_t sample)
{
  DGUARD("Process histogram DDL=0x%04x Address=0x%02x/0x%01x/0x%02x [%03d,%d]",
	 ddl, board, chip, chan, strip, sample);
  // Write out the result 
  float    mean = spectra->Mean();
  float    rms  = spectra->Sigma();
  uint32_t t    = StripSample2Timebin(ddl,board,strip, sample);
  *fCsv << ddl     << ","
	<< board   << "," 
	<< chip    << "," 
	<< chan    << "," 
	<< strip   << "," 
	<< sample  << "," 
	<< t       << ","
	<< mean    << "," 
	<< rms     << ",";
  if (!fFitter || rms < 0.01) {
    *fCsv   << "0,0,0" << std::endl;
    *fChOut << unsigned(mean+.5) << std::endl;
    if (t < fExtraOffset) 
      for (size_t i = 0; i < fExtraOffset; i++) 
	*fChOut << unsigned(mean+.5) << std::endl;
    return true;
  }

  // Fit it.
  fFunction->Reset();
  fFunction->Set(spectra->MaxContent(), mean, rms);
#if 0
  if (fFitter) {
    delete fFitter;
    fFitter = new RcuGsl::NonLinearFitter;
  }
#endif
  try {
    do {
      fFitter->Step();
      // fFitter->Print(0x3);
      // if (verb) f.Print(0x3);
    } while (!fFitter->TestDelta(1e-3,1e-3) && fFitter->Steps() < 300);
    float ped   = fFunction->Parameters()[1];
    float noise = fFunction->Parameters()[2];
    float chi2  = fFitter->ChiSquare() / fFitter->DegreesOfFreedom();
    *fCsv   << ped << "," << noise << "," << chi2 << std::endl;
    *fChOut << unsigned(ped+.5) << std::endl;
    if (t < fExtraOffset) 
      for (size_t i = 0; i < fExtraOffset; i++) 
	*fChOut << unsigned(ped+.5) << std::endl;
  } catch (std::exception& e) {
    std::stringstream s;
    std::cerr << e.what() << std::endl;
    // Fall back to mean of histogram. 
    *fCsv  << "0,0,0" << std::endl;
    *fChOut << unsigned(mean+.5) << std::endl;
    if (t < fExtraOffset) 
      for (size_t i = 0; i < fExtraOffset; i++) 
	*fChOut << unsigned(mean+.5) << std::endl;
    // return false;
    return true;
  }
  return true;
}


//____________________________________________________________________
//
// EOF
//
