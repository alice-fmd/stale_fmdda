// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    FXS.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   File eXchange Server interface 
    @ingroup fmdda_data    
*/
#ifndef FMDDA_FXS
#define FMDDA_FXS
#ifndef RCUDB_SQL
# include <rcudb/Sql.h>
#endif
#ifndef RCUDB_SERVER
# include <rcudb/Server.h>
#endif
#ifndef __SSTREAM__
# include <sstream>
#endif
#ifndef __STRING__
# include <string>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif

namespace RcuDb 
{
  class Server;
}

namespace FmdDA
{
  /** @defgroup fmdda_env Execution environment 
      @ingroup fmdda */
  //__________________________________________________________________
  /** @struct FXS 
      @brief Connectoion to File eXchange Server. 
      @ingroup fmdda_env  */
  struct FXS
  {
    /** Constructor 
	@param url      Database ur;
	@param run      Run number 
	@param detector Detector 
	@param role     Role 
	@param path     Path on FXS */
    FXS(const std::string& url,
	int                run=-1,
	const std::string& detector="",
	const std::string& role="",
	const std::string& path="");
    /** Destructor */
    ~FXS();

    /** Copy a file @a file to FXS giving it the identifier @a id. 
	@param file File to copy from local disk
	@param id Id of copied file in database 
	@return @c 0 on success, error code otherwise */
    int CopyFile(const std::string& file, const std::string& id);
    
  protected:
    /** Connect to database
	@return @c true on success  */
    int Connect();
    /** Get the run number from the environment if not set. 
	@return 0 on success, error code otherwise */
    int GetRun();
    /** Get the detector code from the environment if not set. 
	@return 0 on success, error code otherwise */
    int GetDetector();
    /** Get the role name from the environment if not set. 
	@return 0 on success, error code otherwise */
    int GetRole();
    /** Get the FES path from the environment if not set. 
	@return 0 on success, error code otherwise */
    int GetPath();
    /** Are we initialised ? */
    bool fInit;
    /** Test mode? */
    bool fTest;
    /** The DB connection */
    RcuDb::Server* fServer;
    /** Connection string */
    std::string fCon;
    /** Run number as a string (9 characters, zero padded) */ 
    int fRun;
    /** Detector code */ 
    std::string fDetector;
    /** Role name */ 
    std::string fRole;
    /** The FXS path */ 
    std::string fPath;
  };
}

  
#endif
//
// EOF
//
