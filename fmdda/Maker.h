// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Maker.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Utility class for various FMD analysers
    @ingroup fmdda_data    
*/
#ifndef FMDDA_MAKER
# define FMDDA_MAKER
# include <vector>
# include <string>
# include <iosfwd>
# include <fmdda/AltroDecoder.h>
# include <fmdda/ProgressMeter.h>

namespace FmdDA 
{
  // Forward decl 
  class Reader;
  
  /** @class Maker
      @brief Utility class for various FMD analysers
      @ingroup fmdda
  */
  class Maker : public AltroDecoder
  {
  public:
    /** List of sources */ 
    typedef std::vector<std::string> Sources_t;
    /** Constructor 
	@param src  List of source files to read from 
	@param out  Output file name 
	@param n    Max number of events, or all if < 0
	@param skip Number of events to skip from the beginning
	@param all  Read all or some events.
	@param wait Blocking read of events?
    */
    Maker(const Sources_t& src, 
	  const char*      out,
	  int              n=-1, 
	  unsigned int     skip=0, 
	  unsigned int     baseDDL=0x4e2a, 
	  bool             all=true, 
	  bool             wait=false);

    /** Destructor */
    virtual ~Maker() {}

    /** Loop over the data.  If a non-empty URL is specified, and the
	code is linked to the Rcu++ library, then the first thing done
	is to try to retrieve the needed parameters from the hardware,
	using the specified URL.  If the read fails, or Rcu++ is not
	linked in, then the program will use the setting from the
	user. 
	@param url Connection url. 
	@return @c true on success */
    virtual bool Run(const std::string& url);

    /** @param over Oversampling used when recording the data */
    void SetOverSample(unsigned int over=4);
    /** Set strip range 
	@param min Minimum strip 
	@param max Maximum strip */
    void SetRange(unsigned int min=0, unsigned int max=127);
    /** Set number of events/pulse/strip 
	@param n     Number of events/pulse/strip */
    void SetNSamples(unsigned int n=100);
    /** Set pulse height step size 
	@param step  Step size of pulse */
    void SetPulseStep(unsigned int step=32);

    /** Set the offset 
	@param o offset. */
    void SetOffset(unsigned o=14) { fOffset = o; }
    /** Set the extra off set 
	@param e Extra offset */
    void SetExtraOffset(unsigned e=5) { fExtraOffset = e; }

    /** Get the oversampling rate 
	@param ddl   DDL number 
	@param board Board number 
	@return over sampling rate for @a ddl, @a board */ 
    unsigned int OverSampling(uint32_t ddl, uint32_t board) const;
    /** Get the oversampling rate 
	@param ddl   DDL number 
	@param board Board number 
	@return over sampling rate for @a ddl, @a board */ 
    unsigned int FirstStrip(uint32_t ddl, uint32_t board) const;
    /** Get the oversampling rate 
	@param ddl   DDL number 
	@param board Board number 
	@return over sampling rate for @a ddl, @a board */ 
    unsigned int LastStrip(uint32_t ddl, uint32_t board) const;
    /** Get the oversampling rate 
	@param ddl   DDL number 
	@param board Board number 
	@return over sampling rate for @a ddl, @a board */ 
    unsigned int NSamples(uint32_t ddl, uint32_t board) const;
    /** Get the oversampling rate 
	@param ddl   DDL number 
	@param board Board number 
	@return over sampling rate for @a ddl, @a board */ 
    unsigned int PulseStep(uint32_t ddl, uint32_t board) const;

    /** Address to index 
	@param ddl DDL # 
	@param board Board # 
	@return Internal index in caches */
    unsigned int Address2Index(uint32_t ddl, uint32_t board) const;
  protected:
    /** Check the hardware for values of the various parameters. 
	@param url Connection url. 
	@param ddl DDL number
	@return @c true on success, @c false on error or if the
	program wasn't linked against Rcu++. */
    virtual bool CheckHardware(const std::string& url, uint32_t ddl=0);
    /** Pre process the data collected */
    virtual bool Start() { return true; }
    /** Pre process the data collected */
    virtual bool Event();
    /** Post process the data collected */
    virtual bool End() = 0;
    /** Convert a time bin into sample. 
	@param ddl    DDL number  
	@param board  Board to do the conversion for 
	@param t      Time bin
	@return  Sub-sample corresponding to the time bin */
    unsigned int Timebin2Sample(uint32_t ddl, 
				uint32_t board, 
				uint32_t t) const;
    /** Convert a time bin into strip. 
	@param ddl    DDL number  
	@param board  Board to do the conversion for 
	@param t      Time bin
	@return  strip corresponding to the time bin */
    unsigned int Timebin2Strip(uint32_t ddl, 
			       uint32_t board, 
			       uint32_t t) const;
    /** Convert a time bin into strip,sample pair. 
	@param ddl    DDL number  
	@param board  Board to do the conversion for 
	@param t      Time bin
	@return  strip and sub-sample corresponding to the time bin */
    float Timebin2StripSample(uint32_t ddl, 
			      uint32_t board, 
			      uint32_t t) const;
    /** Convert strip,sample pair to time bin number
	@param ddl    DDL number  
	@param board  Board to do the conversion for 
	@param strip  Strip number
	@param sample Sample number 
	@return corresponding time bin */
    unsigned int StripSample2Timebin(uint32_t ddl, 
				     uint32_t board, 
				     uint32_t strip, 
				     uint32_t sample) const;
    
    /** Reader of raw data */
    Reader* fReader;
    /** Type of vector of unsigned 32 bit integers */
    typedef std::vector<uint32_t> IntVector;
    /** Per board oversampling ratio used when recording the data */
    IntVector fOver;
    /** Per board first strip */
    IntVector fFirst;
    /** Per board last strip */
    IntVector fLast;
    /** Per board number of events/pulse/strip */
    IntVector fIter;
    /** Per board pulse height step size */
    IntVector fStep;
    /** Offset */
    unsigned int fOffset;
    /** Extra offset */
    unsigned int fExtraOffset;
    /** Progress meteter */ 
    ProgressMeter fMeter;
    /** Name of output file */
    std::string  fOutput;
    /** Output stream */ 
    std::ostream*  fCsv;
    /** Maximum size of vectors */ 
    const unsigned int fMaxBoard;
    /** Base of DDL */ 
    const unsigned int fBaseDDL;
  };
}

#endif
//
// EOF
//



