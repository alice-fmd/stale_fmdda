//____________________________________________________________________
//
<<<<<<< AltroDecoder.cxx
// $Id: AltroDecoder.cxx,v 1.7 2009-02-09 23:12:33 hehi Exp $
=======
// $Id: AltroDecoder.cxx,v 1.7 2009-02-09 23:12:33 hehi Exp $
>>>>>>> 1.6
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:26:08 2006
    @brief   Implementation of ALTRO decoder
*/
#include "AltroDecoder.h"
#include "Channel.h"
#include <readraw/Equipment.h>
#include <cmath>
#include <iostream>
#include <iomanip>
#include "DebugGuard.h"

//____________________________________________________________________
const FmdDA::AltroDecoder::uint40_t 
FmdDA::AltroDecoder::fgkTrailerMask = 
  ((FmdDA::AltroDecoder::uint40_t(0x2aaa) << 26) + 
   (FmdDA::AltroDecoder::uint40_t(0xa) << 12));

//____________________________________________________________________
FmdDA::AltroDecoder::AltroDecoder()
  : fIsValid(false),
    fDDL(0),
    fCurrent(0), 
    fCur(0), 
    fStart(0), 
    fN(0),
    fDecoded(0),
    fDecodedCur(0)
{}

//____________________________________________________________________
FmdDA::AltroDecoder::~AltroDecoder() 
{ 
  if (fCurrent) delete fCurrent; 
}

//____________________________________________________________________
bool
FmdDA::AltroDecoder::Visit(const ReadRaw::Event& event)
{
  DGUARD("visiting event");
  return ReadRaw::Visitor::Visit(event);
}

//____________________________________________________________________
bool
FmdDA::AltroDecoder::Visit(const ReadRaw::Equipment& equipment)
{
  // DGUARD("visiting equipment # %d", equipment.Id());
  GotDDL(equipment.Id());
  return ReadRaw::Visitor::Visit(equipment);
}

//____________________________________________________________________
bool
FmdDA::AltroDecoder::Visit(const uint32_t* data, 
			   const uint32_t  size) 
{
  // DGUARD("visiting data %p of size %d", data, size);
  // Printer::Visit(data);
  // fArray->Clear();
  if (!DecodeEvent(data, size)) {
    std::cout << "FmdDA::AltroDecoder::Visit(data): Failed to decode" 
	      << std::endl;
    return false;
  }
  return true;
}

//____________________________________________________________________
void
FmdDA::AltroDecoder::Clear()
{
  fIsValid = false;
  // fCurrent = 0;
  // for (ChannelMap::iterator i = fMap.begin(); i != fMap.end(); ++i) 
  //   delete  i->second;
  // fMap.clear();
}

//____________________________________________________________________
void
FmdDA::AltroDecoder::DecodeError(int err)
{
  switch (err) {
  case kEOD:        std::cerr << "End of data"       << std::endl; break;
  case kRead:	    std::cerr << "Inconsistent read" << std::endl; break;
  case kBadValue:   std::cerr << "Invalid value"     << std::endl; break;
  case kBadTrailer: std::cerr << "Not a trailer"     << std::endl; break;
  case kBadFill:    std::cerr << "Not a fill word"   << std::endl; break;
  case kTooLong:    std::cerr << "Read too long"     << std::endl; break;
  default:          std::cerr << "Unknown error " << err << std::endl; break;
  }
}

    
//____________________________________________________________________
bool
FmdDA::AltroDecoder::DecodeEvent(const uint32_t* data, 
				 const uint32_t  size) 
{
  // DGUARD("decode event data  %p of size %d", data, size);
  Clear();
  uint32_t lsize  = sizeof(uint32_t);
  uint32_t nw40   = size * lsize / 5;
  uint32_t remain = size * lsize * 8 - nw40 * 40;
  fCur          = (unsigned char*)(&(data[size]));
  fStart        = (unsigned char*)(&(data[0]));
  fN            = 0;
  
  // If we got less than a full word in the remainder, then the last
  // 40 it word is a partial extra word, so we disregard that. 
  if (remain < 32) nw40--;

  // Read in the extra words.  Nothing is done to them. 
  uint32_t nextra = size * lsize - nw40 * 40 / 8;
  // std::vector<unsigned char> extra;
  for (uint32_t i = 0; i < nextra; i++) {
    fCur--;
    // unsigned char t = *fCur;
    // extra.push_back(t);
  }
  int ret;
  while ((ret = DecodeChannel()) > 1);
  if (ret < 0 && ret != -kEOD) {
    std::cout << "DecodeChannel returned " << ret << std::endl;
    DecodeError(-ret);
    return false;
  }
  
  return true;
}

//____________________________________________________________________
int
FmdDA::AltroDecoder::GetNext10bitWord() 
{
  
  if (fDecoded) {
    if (fDecodedCur == 0) return -1;
    fDecodedCur--;
    return ((*fDecoded)[fDecodedCur]) & 0x3ff;
  }
  
  static uint40_t  last40;
  static uint32_t neod = 0;
  if (fCur <= fStart && fN == 0) { 
    if (neod > 0) return -kTooLong;
    neod++;
    return -kEOD;
  }
  neod = 0;
  

  if (fN == 0) {
    last40 = 0;
    fN     = 4;
    // Read in a 40bit word 
    for (int i = 4; i >= 0; i--) {
      fCur--;
      if (fCur <= fStart) return kRead;
      last40 += (uint40_t(*fCur & 0xff) << (i * 8));
    }
  }
  fN--;
  int ret = (last40 >> (fN * 10)) & 0x3ff;
  if (ret < 0 || ret > 0x3ff) return kBadValue;
  return ret;
}

//____________________________________________________________________
int 
FmdDA::AltroDecoder::DecodeTrailer(uint32_t& addr, uint32_t& last)
{
  // DGUARD("decode trailer");
  uint40_t trailer = 0;

  int ret = 0;
  for (uint32_t i = 4; i > 0; i--) {
    int data =  GetNext10bitWord();
    if (data < 0) return data;
    trailer  += (uint40_t(data)) << ((i-1)*10);
    ret++;
    // std::cout << "Trailer word " << i << " 0x" << std::hex 
    //  << std::setw(3) << data << std::dec << std::endl;
  }
  if (!IsTrailer(trailer)) {
    std::cerr << "Word  0x" << std::hex << std::setw(10) 
	      << std::setfill('0') << trailer << " (0x" 
	      << fgkTrailerMask << " 0x" << std::setw(10) 
	      << (trailer  & fgkTrailerMask) << ")"
	      << std::dec << std::setfill(' ')
	      << " does not fit a trailer"  << std::endl;
    return -kBadTrailer;
  }
  addr           =  (trailer & 0xfff);
  last           =  (trailer >> 16) & 0x3ff;
  uint32_t nfill   = (last % 4 == 0 ? 0 : 4 - last % 4);
  for (uint32_t i = 0; i < nfill; i++) {
    int fill = GetNext10bitWord();
    ret++;
    if (fill != 0x2aa) {
      uint32_t board, chip, channel;
      DecodeAddress(trailer, board, chip, channel);
      std::cerr << "Invalid fill 0x" <<  std::hex << fill << std::dec 
		<< " at " << i << " b" 
		<< std::setfill('0') 
		<< std::setw(2) << board   << "a" 
		<< std::setw(1) << chip    << "c" 
		<< std::setw(2) << channel 
		<< std::setfill(' ') << " w/" 
		<< std::setw(4) << last << " 10bit words"  << std::endl;
      // return -kBadFill;
    }
  }
  return ret;
}

//____________________________________________________________________
void
FmdDA::AltroDecoder::DecodeAddress(const uint32_t addr, uint32_t& board, 
				   uint32_t& chip, uint32_t& channel) const
{
  channel  =  (addr >>  0) & 0x00f;
  chip     =  (addr >>  4) & 0x007;
  board    =  (addr >>  7) & 0x01f;
}  

//____________________________________________________________________
void
FmdDA::AltroDecoder::GotDDL(uint32_t ddl)
{
  fDDL = ddl;
}

//____________________________________________________________________
bool
FmdDA::AltroDecoder::GotChannel(uint32_t ddl, 
				uint32_t board, 
				uint32_t chip, 
				uint32_t channel, 
				uint32_t last)
{
  DGUARD("Got channel DDL=0x%04x Address=0x%02x/0x%01x/0x%02x Last=%d",
	 ddl, board, chip, channel, last);
  if (!fCurrent) fCurrent = new Channel;
  else           fCurrent->Clear();
  fIsValid           = true;
  fCurrent->fDDL     = ddl;
  fCurrent->fBoard   = board;
  fCurrent->fChip    = chip;
  fCurrent->fChannel = channel;
  fCurrent->fLast    = last;
  return true;
}

//____________________________________________________________________
bool
FmdDA::AltroDecoder::GotData(uint10_t t, uint10_t adc)
{
  // DGUARD("got ADC counts 0x%03x at t=%d", adc, t);
  if (!fCurrent && !fIsValid) return false;
  if (t < fCurrent->fM) fCurrent->fM = t;
  if (t > fCurrent->fN) fCurrent->fN = t;
  fCurrent->fData[t] = adc;
  return true;
}

//____________________________________________________________________
int
FmdDA::AltroDecoder::DecodeChannel(const uint10_vector_t& data,
				   volatile uint32_t& size)
{
  // DGUARD("Decode channel in %p of size %d", &(data[0]), size);
  fDecoded    = &data;
  fDecodedCur = size;
  int ret     = DecodeChannel();
  fDecoded    = 0;
  return ret;
}

//____________________________________________________________________
int
FmdDA::AltroDecoder::DecodeChannel()
{
  // DGUARD("Decode channel");
  uint32_t addr, channel, chip, board, last;
  int ret  = 0;
  // fCurrent = 0;
  fIsValid = false;
  if ((ret = DecodeTrailer(addr, last)) < 0) {
    if (ret != -kEOD) 
      std::cout << "FmdDA::AltroDecoder::DecodeChannel() failed at trailer" 
		<< std::endl;
    return ret;
  }
  DecodeAddress(addr, board, chip, channel);
  bool call = GotChannel(fDDL, board, chip, channel, last); 
  
  int rem   = 0;
  int t     = 0;
  for (uint32_t i = 0; i < last; i++) {
    if (rem == 0) {
      // New bunch 
      rem =  GetNext10bitWord()-2;
      if (rem < 0) return rem;
      t   =  GetNext10bitWord();
      if (t < 0) return t;
      i   += 2;
      ret += 2;
    }
    t--;
    int data = GetNext10bitWord();
    if (data < 0) return data;
    ret++;
    if (call) call = GotData(t, uint10_t(data));
    rem--;
  }
  // std::cout << "Returning " << ret << std::endl;
  return ret;
}

//____________________________________________________________________
//
// EOF
//
