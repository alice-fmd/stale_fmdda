// -*- mode: C++ -*-
//____________________________________________________________________
//
<<<<<<< AltroDecoder.h
// $Id: AltroDecoder.h,v 1.6 2009-02-09 23:12:33 hehi Exp $
=======
// $Id: AltroDecoder.h,v 1.6 2009-02-09 23:12:33 hehi Exp $
>>>>>>> 1.5
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jun 26 20:10:43 2006
    @brief   Declaration of decoder or ALTRO data 
*/
#ifndef FMDDA_ALTRODECODER_H
#define FMDDA_ALTRODECODER_H
#include <vector>
#include <readraw/Visitor.h>
#include <readraw/Types.h>


namespace FmdDA
{
  /** Forward declaration */
  class Channel;
  
  /** @struct AltroDecoder AltroDecoder.h <fmdda/AltroDecoder.h>
      @brief Decode ALTRO data 
      @ingroup fmdda_raw */
  struct AltroDecoder : public ReadRaw::Visitor
  {
    /** Type of 16bit unsigned integer */
    typedef ReadRaw::uint8_t uint8_t;
    /** Type of 16bit unsigned integer */
    typedef ReadRaw::uint16_t uint16_t;
    /** Type of 32bit unsigned integer */
    typedef ReadRaw::uint32_t uint32_t;
    /** Type of 64bit unsigned integer */
    typedef ReadRaw::uint64_t uint64_t;
    /** Type of 10bit unsigned integer */
    typedef ReadRaw::uint10_t uint10_t;
    /** Type of 40bit unsigned integer */
    typedef ReadRaw::uint40_t uint40_t;
    
    /** Type of vector of 10 bit words */
    typedef std::vector<uint10_t>  uint10_vector_t;
    /** Type of vector of 32 bit words */
    typedef std::vector<uint32_t> uint32_vector_t;
    /** Error codes */ 
    enum {
      kEOD  = 1,
      kRead, 
      kBadValue, 
      kBadTrailer, 
      kBadFill,
      kTooLong
    };
    
    /** construcutor */
    AltroDecoder();
    /** Destructor */
    virtual ~AltroDecoder();

    /** @{ 
	@name Visitor member function */
    /** Visit an event.  Calls Visit on header, and header of each
        sub-event.
        @param event The Event object to visit.
        @return @c true on success, @c false otherwise  */
    virtual bool Visit(const ReadRaw::Event&  event);
    /** Visit an equipment.  Calls Visit on the common data header,
        and the data of the equipment. 
        @param equipment Equipment object to visit.
        @return @c true on success, @c false otherwise */
    virtual bool Visit(const ReadRaw::Equipment& equipment);
    /** Print the raw data from an equipment. 
	@param data Raw data from the equipment, packed in 32bit
	words. 
	@param size Number of 32bit words in @a data 
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const uint32_t* data, 
		       const uint32_t  size);
    /** @}*/ 

    /** Clear internal array */
    virtual void Clear();

    /** @{ 
	@name Decoding member functions */
    /** Decode an event
	@param data Array of 32bit words 
	@param size Number of 32bit words in @a data 
	@return @c true on success, @c false otherwise */
    virtual bool DecodeEvent(const uint32_t* data,
			     const uint32_t  size);
    /** Decode a channel 
	@param data Array of 10bit words 
	@param size Number of words in this channel.  On return, this
	should be 0.  
	@return @c true on success, @c false otherwise */
    virtual int DecodeChannel(const uint10_vector_t& data, 
			      volatile uint32_t& size);
    /** Decode a channel 
	@return @c true on success, @c false otherwise */
    virtual int DecodeChannel();
    /** Decode the trailer of a channel 
	@param addr    On return, the full channel address
	@param last    Index of last data word in @a data 
	@return @c true if the trailer and fill words are valid,
	otherwise @c false */
    virtual int DecodeTrailer(uint32_t& addr, uint32_t& last);
    /** Decode the full channel address given in @a addr into it's
	components. 
	@param addr    Full channel address 
	@param board   On return, the board number 
	@param chip    On return, the chip number 
	@param channel On return, the channel number */
    void DecodeAddress(const uint32_t addr, uint32_t& board, 
		       uint32_t& chip, uint32_t& channel) const; 
    /** @} */
  protected:
    /** Trailer mask */ 
    static const uint40_t fgkTrailerMask;
    /** @{ 
	@name Error handling and checks */
    /** Check if the 40 bit word @a w matches a trailer 
	@param w 40bit word to test
	@return  @c true if @a w matches a trailer */
    bool IsTrailer(const uint40_t& w) const 
    { 
      return (w & fgkTrailerMask) == fgkTrailerMask; 
    }
    /** Decode the error. 
	@param err Error code */
    virtual void DecodeError(int err);
    /** @} */

    /** @{ 
	@name Handler member functions */
    /** Called when we get a new DDL. Set the internal member fDDL to
	the passed value 
	@param ddl New DDL value */ 
    virtual void GotDDL(uint32_t ddl);
    /** Called when we got a new channel.   Derived classes can
	override this member function to process the trailer if
	needed.  Note, that the overridding member function of the

	derived class should call this function. 
	@param ddl     DDL number
	@param board   FEC address
	@param chip    ALTRO adddress
	@param channel ALTRO channel number
	@param last    Number of words in data 
	@return @c true means continue decoding this channel */
    virtual bool GotChannel(uint32_t ddl, 
			    uint32_t board, 
			    uint32_t chip, 
			    uint32_t channel, 
			    uint32_t last);
    
    /** Called when we got data for a time bin for the current
	channel. Derived classes can override this member function to
	process the trailer if needed.  Note, that the overridding
	member function of the derived class should call this
	function.
	@param t   Timebin.  Note, @c t==15 corresponds to the time
	of the level 1 trigger 
	@param adc ADC value at @a t. 
	@return @a true means continue decoding this channel */
    virtual bool GotData(uint10_t t, uint10_t adc);
    /** Read one 10bit word from input
	@return -1 if case of no more data, otherwise, the 10bit word
	*/
    int GetNext10bitWord();
    /** @} */



    /** Valid channel data */ 
    bool fIsValid;
    /** Current DDL value */ 
    uint32_t fDDL;
    /** Current channel, if any */ 
    Channel* fCurrent;
    /** Pointer to current position in input */
    unsigned char* fCur;
    /** Pointer to head of input */ 
    unsigned char* fStart;
    /** Number of 10 bit words in 40 bit buffer */
    uint32_t fN;
    /** Pointer to an array of decoded 10bit words */ 
    const uint10_vector_t* fDecoded;
    /** Size of decoded data vector */
    uint32_t fDecodedCur;
  };
}

#endif 
//____________________________________________________________________
//
// EOF
//


  
