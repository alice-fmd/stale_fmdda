//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef FMDDA_CHANNEL_H
# include <fmdda/Channel.h>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif

//____________________________________________________________________
typedef unsigned long long  w40_t;
static w40_t trailerMask = ((w40_t(0x2aaa) << 26) + (w40_t(0xa) << 12));

//____________________________________________________________________
FmdDA::Channel::Channel() 
  : fBoard(0),
    fChannel(0),
    fChip(0), 
    fLast(0),
    fM(1024),
    fN(0)
{
  Clear();
}

//____________________________________________________________________
void
FmdDA::Channel::Clear()
{
  fBoard   = 0;
  fChannel = 0;
  fChip    = 0; 
  fLast    = 0;
  fM       = 1024;
  fN       = 0;
  for (int i = 0; i < 1024; i++) fData[i] = 0;
}

//____________________________________________________________________
FmdDA::Channel::Channel(const unsigned int* data, unsigned int size) 
  : fBoard(0),
    fChannel(0),
    fChip(0), 
    fLast(0),
    fM(1024), 
    fN(0)
{
  w40_t trailer = ((w40_t(data[size - 1]) << 30)
		   + (w40_t(data[size - 2]) << 20) 
		   + (w40_t(data[size - 3]) << 10) 
		   +  data[size - 4]);
  if ((trailer & trailerMask) != trailerMask) {
    std::cerr << "Invalid trailer 0x" << std::hex << std::setw(10) 
	      << std::setfill('0') << trailer << " (0x" 
	      << std::setw(10) << trailerMask << ")" << std::dec 
	      << std::setfill(' ') << std::endl;
    return;
  }
  fChannel = trailer         & 0xf;
  fChip    = (trailer >> 4)  & 0x3;
  fBoard   = (trailer >> 7)  & 0x1f;
  fLast    = (trailer >> 16) & 0x3ff;
  unsigned int nfill = (fLast % 4 == 0 ? 0 : 4 - fLast %  4);
  int i      = size - 5;
  if (i < 0) {
    std::cerr << "No data words!" << std::endl;
    return;
  }
  for (unsigned int j = 0; j < nfill; j++, i--) {
    if (data[i] != 0x2aa) {
      std::cerr << "Invalid fill " << j << "th word 0x" 
		<< std::hex << data[i] << std::dec << std::endl;
      return;
    }
  }
  while (i >= 0) {
    unsigned int l = data[i]; i--;
    unsigned int t = data[i]; i--;

    // std::cout << "New bunch ending at t=" << t << " of length " << l 
    //           << std::endl;
    for (int j = 0; j < int(l - 2); j++, i--) {
      if (i < 0 || i >= int(size)) {
	std::cerr << "Index " << i << " larger than size " << size 
		  << " of data!" << std::endl;
	break;
      }
      if (t - j > fN) fN = t - j;
      if (t - j < fM) fM = t - j;
      if (data[i] > 1024 || data[i] < 0) 
	std::cerr << "Invalid data entry " << std::setw(3) << i 
		  << ": " << data[i] << std::endl;
      // std::cout << "Putting " << data[i] << " @ " << t - j << std::endl;
      fData[t - j] = data[i];
    }
  }
  for (i = fN; i < 1024; i++) fData[i] = 0;
  for (i = fM-1; i >= 0; i--) fData[i] = 0;
}

//____________________________________________________________________
FmdDA::Channel::~Channel() 
{}

//____________________________________________________________________
void
FmdDA::Channel::Print() const
{
  std::cout << "Channel: " 
	    << std::setw(2) << fBoard << "/" 
	    << std::setw(2) << fChip << "/" 
	    << std::setw(4) << fChannel << " (last " << fLast << "): "
	    << std::flush;
  for (int i = fM; i <= fN; i++) {
    if (i % 6 == 0) std::cout << std::endl;
    std::cout << "  " << std::setw(3) << i 
	      << ": " << std::setw(4) << fData[i] << std::flush;
  }
  std::cout << std::endl;
}
  
//____________________________________________________________________
//
//EOF
//

