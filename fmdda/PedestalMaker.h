// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Class to find pulser spectrum. 
    @ingroup fmdda_data    
*/
#ifndef FMDDA_PEDESTALMAKER
#define FMDDA_PEDESTALMAKER
#ifndef __CCTYPE__
# include <cctype>
#endif
#ifndef FMDDA_SPECTRA
# include <fmdda/Spectra.h>
#endif
#ifndef FMDDA_Maker
# include <fmdda/Maker.h>
#endif

// Forward decls 
namespace RcuGsl
{
  class Fitter;
  class Gaus;
}

namespace FmdDA 
{
  /** @defgroup peds Pedestal extraction 
      @ingroup fmdda */

  /** @class PedestalMaker
      @brief Class to find ADC spectra for each strip/sample
      @ingroup peds
  */
  class PedestalMaker : public Maker
  {
  public:
    /** list of sources */ 
    typedef Maker::Sources_t Sources_t;

    /** Start of job.
	@param src   Input source
	@param out   Output file
	@param chdir Output directory for channel files
	@param n     # of events to process 
	@param skip  # of evenns to skip
	@param all   Analyse all events 
	@param wait  Wait for data */
    PedestalMaker(const Sources_t&  src, 
		  const char*       out, 
		  const char*       chdir,
		  int               n=-1, 
		  unsigned int      skip=0, 
		  unsigned int      baseDDL=0x4e2a, 
		  bool              all=true, 
		  bool              wait=false);
    /** Destructor */
    virtual ~PedestalMaker();
    /** Whether we should fit the data */ 
    virtual void SetFit(bool use=true);
    
    /** @{ 
	@name Handler routines, reimplementation of AltroDecoder
	member functions */
    /** Called when ever we get a new channel from reader.
	@param ddl     DDL number
	@param board   Board number 
	@param chip    Chip number 
	@param channel Channel number 
	@param last    Last ADC word 
	@return @c true, means continue with this channel */
    virtual bool GotChannel(uint32_t ddl, 
			    uint32_t board, 
			    uint32_t chip, 
			    uint32_t channel, 
			    uint32_t last);
    /** Called when ever we get a new channel from reader.
	@param c      Channel object
	@param hasAll @c true if @a c has all information.  
	@return @c true, means continue with this channel */
    virtual bool GotChannel(FmdDA::Channel& c, bool hasAll);
    /** Called when ever we get a new ADC value for the current
	channel. 
	@param t   Time bin
	@param adc ADC value 
	@return  @c true, means continue with this channel */
    virtual bool GotData(uint10_t t, uint10_t adc);
    /** @} */
  protected:
    /** Process all strip spectra. */
    virtual bool Start();
    /** Process all strip spectra. */
    virtual bool End();
    /** Process a final spectra 
	@param spectra Spectra to process 
	@param ddl     DDL #
	@param board   Board #
	@param chip    Chip (ALTRO) # 
	@param chan    Channel (VA1) # 
	@param strip   Strip # 
	@param sample  Sample # */
    virtual bool ProcessSpectrum(RcuGsl::Histogram* spectra, 
				 uint32_t ddl,
				 uint32_t board, 
				 uint32_t chip, 
				 uint32_t chan, 
				 uint32_t strip, 
				 uint32_t sample);
    /** Top-level of cache */
    Spectra::Fmd fTop;
    /** Current RCU */
    Spectra::Rcu* fCurRcu;
    /** Current channel */ 
    Spectra::Board* fCurBoard;
    /** Current channel */ 
    Spectra::Chip* fCurChip;
    /** Current channel */ 
    Spectra::Chan* fCurChan;
    /** The fitter */ 
    RcuGsl::Fitter*  fFitter;
    /** The function */ 
    RcuGsl::Gaus* fFunction;
    /** The directory to put the channel files in */
    std::string    fChDir;
    /** The current channel output file */ 
    std::ofstream* fChOut;
  };
}
#endif
//
// EOF
//

