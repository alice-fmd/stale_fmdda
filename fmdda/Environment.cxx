//
// $Id: Environment.cxx,v 1.3 2009-02-09 23:12:33 hehi Exp $
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   File eXchange Server interface 
*/
#include "config.h"
#include "Environment.h"

FmdDa::Environment* FmdDa::Environment::fgInstance = 0;

FmdDa::Environment&
FmdDa::Environment::Instance()
{
  if (!fgInstance) fgInstance = new Environment;
  return fgInstance;
}

void 
FmdDa::Environment::CheckEnv(const std::string& name, std::string& ret)
{
  const char* val = getenv(name.c_str());
  ret = (val ? val : "");
}
  
FmdDa::Environment::Environment()
  : fRun(0),
    fIsTest(false)
{
  Read();
}

FmdDa::Environment::Read()
{  
  if (getenv("DAQDA_TEST_DIR")) {
    fIsTest   = true;
    fPath     = getenv("DAQDA_TEST_DIR");
    fRun      = 0;
    fRole     = "test";
    fDetector = "DAQ";
    return;
  }
  CheckEnv("DATE_INFOLOGGER_MYSQL_USER", fUser);
  CheckEnv("DATE_INFOLOGGER_MYSQL_PWD",  fPassword);
  CheckEnv("DATE_INFOLOGGER_MYSQL_HOST", fHost);
  CheckEnv("DATE_INFOLOGGER_MYSQL_DB",   fDatabase);

  std::string srun;
  CheckEnv("DATE_RUN_NUMBER", srun);
  if (!srun.empty()) {
    std::stringstream str(srun);
    str >> fRun;
  }
  CheckEnv("DATE_ROLE_NAME",     fRole);
  CheckEnv("DATE_DETECTOR_CODE", fDetector);
}

//
// EOF
//





  






