// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Gains.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Cache of gain scan data 
    @ingroup fmdda_data    
*/
#ifndef FMDDA_DATA_GAINS
#define FMDDA_DATA_GAINS
#ifndef FMDDA_CONTAINER
# include <fmdda/Container.h>
#endif

namespace RcuGsl 
{
  class UnbinnedData;
  class Histogram;
}

namespace FmdDA 
{
  namespace Gains 
  {
    struct Rcu;
    struct Board;
    struct Chip;
    struct Chan;
    struct Strip;
    
    /** @defgroup gain_spectra Gain spectra 
	@ingroup gains 
	@ingroup fmdda_spectra */
    //__________________________________________________________________
    /** Container of Rcu information 
	@ingroup gain_spectra
    */
    struct Fmd : public FmdDA::Container<Rcu>
    {
      /** Type of base class */
      typedef FmdDA::Container<Rcu> Base_t;
      /** Constructor */
      Fmd() : Base_t(0, 3) {}
      /** @return Get name */
      const char* GetName() const { return "fmd"; }
      /** Get a board, or if it doesn't exist, make a new one. 
	  @param id Id of board 
	  @return Pointer to board containeer */
      Rcu* GetOrAdd(unsigned int id);
      /** Put information in file */
      void WriteOut();
      /** Fill histograms 
	  @param ddl 	  DDL number 
	  @param board    Board number
	  @param chip     Chip number
	  @param channel  Channel number 
	  @param strip    Strip number 
	  @param pulse    Pulse value
	  @param adc      ADC Value */
      virtual void Fill(unsigned int ddl,
			unsigned int board,   unsigned int chip, 
			unsigned int channel, unsigned int strip,     
			unsigned int pulse,   unsigned int adc);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This is used for the
	  final progress bar. 
	  @param e element to do counting for 
	  @return Total count of elements in @a e and it's daughters */
      virtual size_t CountElem(Elem_t* e) const;
      // ClassDef(Rcu,1);
    };
    //__________________________________________________________________
    /** Container of Rcu information 
	@ingroup gain_spectra
    */
    struct Rcu : public FmdDA::Container<Board>
    {
      /** Type of base class */
      typedef FmdDA::Container<Board> Base_t;
      /** Constructor */
      Rcu(unsigned int id, Fmd& fmd) : Base_t(id,17), fFmd(&fmd) {}
      /** @return Get name */
      const char* GetName() const;
      /** Get a board, or if it doesn't exist, make a new one. 
	  @param id Id of board 
	  @return Pointer to board containeer */
      Board* GetOrAdd(unsigned int id);
      /** Put information in file */
      void WriteOut();
      /** Fill histograms 
	@param board    Board number
	@param chip     Chip number
	@param channel  Channel number 
	@param strip    Strip number 
	@param pulse    Pulse value
	@param adc      ADC Value */
      virtual void Fill(unsigned int board,   unsigned int chip, 
			unsigned int channel, unsigned int strip,     
			unsigned int pulse,   unsigned int adc);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This is used for the
	  final progress bar. 
	  @param e element to do counting for 
	  @return Total count of elements in @a e and it's daughters */
      virtual size_t CountElem(Elem_t* e) const;
      /** Cache of name */
      mutable std::string fName;
      /** up link */
      Fmd* fFmd;
      // ClassDef(Rcu,1);
    };
	
    //__________________________________________________________________
    /** Container of Board information 
	@ingroup gain_spectra
    */
    struct Board : public FmdDA::Container<Chip>
    {
      /** Type of base class */
      typedef FmdDA::Container<Chip> Base_t;
      /** Constructor */
      Board() {}
      /** Constructor 
	  @param id   Board number
	  @param rcu Controlling RCU */
      Board(unsigned int id, Rcu& rcu) : Base_t(id,3), fRcu(&rcu) {}
      /** @return Get name */
      const char* GetName() const;
      /** Get a chip, or if it doesn't exist, make a new one. 
	  @param id Id of chip 
	  @return Pointer to chip containeer */
      Chip* GetOrAdd(unsigned int id);
      /** Fill histograms 
	@param chip     Chip number
	@param channel  Channel number 
	@param strip    Strip number
	@param pulse    Pulse height
	@param adc      ADC Value */
      virtual void Fill(unsigned int chip,  unsigned int channel, 
			unsigned int strip, unsigned int pulse,
			unsigned int adc);
    protected: 
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This is used for the
	  final progress bar. 
	  @param e element to do counting for 
	  @return Total count of elements in @a e and it's daughters */
      virtual size_t CountElem(Elem_t* e) const;
      /** Up link */
      Rcu* fRcu;
      /** Cache of name */
      mutable std::string fName;
      // ClassDef(Board,1);
    };
    

    //__________________________________________________________________
    /** Container of Chip information 
	@ingroup gain_spectra
    */
    struct Chip : public FmdDA::Container<Chan>
    {
      /** Type of base class */
      typedef FmdDA::Container<Chan> Base_t;
      /** Constructor */
      Chip() {}
      /** Constructor 
	  @param n Id of chip 
	  @param b Back link to board */
      Chip(unsigned int n, Board& b) : Base_t(n,16), fBoard(&b) {}
      /** @return Get name */
      const char* GetName() const;
      /** @return reference to mother board */
      const Board& Mother() const { return *fBoard; }
      /** Get a channel, or if it doesn't exist, make a new one. 
	  @param id Id of chan 
	  @return Pointer to chan containeer */
      Chan* GetOrAdd(unsigned int id);
      /** Fill histograms 
	  @param channel  Channel number 
	  @param strip    Strip number
	  @param pulse    Pulse height
	  @param adc      ADC Value */
      virtual void Fill(unsigned int channel, unsigned int strip,
			unsigned int pulse,   unsigned int adc);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This is used for the
	  final progress bar. 
	  @param e element to do counting for 
	  @return Total count of elements in @a e and it's daughters */
      virtual size_t CountElem(Elem_t* e) const;
      /** Back link to board */
      Board* fBoard;
      /** Cache of name */
      mutable std::string fName;
      // ClassDef(Chip,1);
    };

    //__________________________________________________________________
    /** Container of Channel information 
	@ingroup gain_spectra
    */
    struct Chan : public FmdDA::Container<Strip>
    {
      /** Type of base class */                     
      typedef FmdDA::Container<Strip> Base_t;
      /** Constructor */
      Chan() {}
      /** Constructor 
	  @param n Id of channel 
	  @param chip Back link to chip */
      Chan(unsigned int n, Chip& chip) : Base_t(n,127), fChip(&chip) {}
      /** @return Get name */
      const char* GetName() const;
      /** @return reference to mother board */
      const Chip& Mother() const { return *fChip; }
      /** Fill an ADC value into the appropriate spectrum
	  @param strip Strip number 
	  @param pulse Pulse height
	  @param data  ADC value */
      void Fill(unsigned int strip, unsigned int pulse, unsigned int data);
      /** Hide member function */
      Strip* GetOrAdd(unsigned int id);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.
	  @param e Element to count
	  @return always 1 */
      virtual size_t CountElem(Elem_t* e)  const;
      /** Back link to chip */
      Chip* fChip;
      /** Cache of name */
      mutable std::string fName;
      // ClassDef(Chan,1);
    };  
    //__________________________________________________________________
    /** Container of Channel information 
	@ingroup gain_spectra
    */
    struct Strip : public FmdDA::Container<RcuGsl::Histogram>
    {
      /** base class type */
      typedef FmdDA::Container<RcuGsl::Histogram> Base_t;
      /** Constructor */
      Strip() {}
      /** Constructor 
	  @param n Id of stripnel 
	  @param chan Back link to chip */
      Strip(unsigned int n, Chan& chan) : Base_t(n,10), fChan(&chan) {}
      /** @return Get name */
      const char* GetName() const;
      /** @return reference to mother board */
      const Chan& Mother() const { return *fChan; }
      /** @return channel number */
      unsigned int ChanNo() const { return Mother().Id(); }
      /** @return chip number */ 
      unsigned int ChipNo() const { return Mother().Mother().Id(); }
      /** @return Board number */ 
      unsigned int BoardNo() const { return Mother().Mother().Mother().Id(); }
      /** Fill an ADC value into the appropriate spectrum
	  @param pulse Pulse height number
	  @param adc   ADC value */
      void Fill(unsigned int pulse, unsigned int adc);
      /** Clear (delete) all contained objects */
      virtual void Clear();
      /** Reset contents */
      virtual void Reset();
      /** @return number of contained object (recursive) */
      virtual size_t Count() const;
      /** Write histograms to disk. */
      void WriteOut();
      /** Hide member function */
      RcuGsl::Histogram* GetOrAdd(unsigned int id);
      /** Get the graph of the data */
      RcuGsl::UnbinnedData* MakeData();
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This just returns 1. 
	  @return always 1 */
      virtual size_t CountElem(Elem_t*)  const { return 1; }
      /** Back link to chip */
      Chan* fChan;
      /** Cache of name */
      mutable std::string fName;
      // ClassDef(Strip,1);
    };      
  }
}
#endif
//
// EOF
//
