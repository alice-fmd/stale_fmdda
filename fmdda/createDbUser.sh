#!/bin/sh
host=localhost
user=daq
passwd=daq
db=DATE_LOG

usage()
{
    cat <<EOF
Usage: $0 [OPTIONS]

Options: 
	-h,--help		This help 
	-H,--host=HOST		Host name of server 	[$host]
	-U,--user USER		DB user name 		[$user]
	-p,--password=PASSWORD	DB user password	[$passwd]
	-d,--db=DB		DB name			[$db]
EOF
    exit 0;
}

while test $# -gt 0 ; do 
    case $i in 
	--*=*)	opt=`echo $1 | sed 's/=.*//'` 
		arg=`echo $1 | sed 's/[^=]*=//'` 
		;;
	--*)	opt=$1 
	    	arg=
		;;
	-*)	opt=$1 
	    	arg=$2 
		shift 
		;;
    esac

    case $opt in 
	-h|--help) 	usage 		;; 
	-H|--host) 	host=$arg 	;; 
	-u|--user) 	user=$arg 	;; 
	-p|--password) 	passwd=$arg 	;;
	-d|--db)	db=$arg		;;
	*)
	    echo "Unknown option '$opt'" > /dev/stderr 
	    exit 1
	    ;;
    esac
    shift
done

mysql -f -u root -p -h $host mysql <<EOF
DELETE FROM user WHERE User='$user';
DELETE FROM db   WHERE User='$user';
INSERT INTO user (Host, User, Password, Select_priv, Insert_priv, Update_priv,
		  Delete_priv, Create_priv, Drop_priv, Reload_priv, 
		  Shutdown_priv, Process_priv, File_priv, Grant_priv,
		  References_priv, Index_priv, Alter_priv) 
	   VALUES('$host', '$user', PASSWORD('$passwd'), 'N', 'N', 'N',
		  'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
INSERT INTO db (Host, Db, User, Select_priv, Insert_priv, Update_priv,
		Delete_priv, Create_priv, Drop_priv, Grant_priv,
		References_priv, Index_priv, Alter_priv)
	VALUES ('$host', '$db', '$user', 'Y', 'Y', 'N', 'N', 'N', 'N', 
		'N', 'N', 'N', 'N');
INSERT INTO db (Host, Db, User, Select_priv, Insert_priv, Update_priv,
		Delete_priv, Create_priv, Drop_priv, Grant_priv,
		References_priv, Index_priv, Alter_priv)
	VALUES ('%', '$db', '$user', 'Y', 'Y', 'N', 'N', 'N', 'N', 
		'N', 'N', 'N', 'N');
FLUSH PRIVILEGES;
DROP DATABASE $db; 
CREATE DATABASE $db;
CONNECT $db;
CREATE TABLE daqFES_files (run		INT, 
			   detector 	VARCHAR(16),
			   fileId	VARCHAR(256),
			   filePath     TEXT,
			   size		INT,
			   time_created	INT,
			   DAQsource	TEXT,
			   fileCheckSum	TEXT,
			   INDEX(run),
			   INDEX(detector),
			   INDEX(fileId));
EOF
