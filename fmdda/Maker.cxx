// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Maker.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   Class to find pulser spectrum. 
    @ingroup fmdda_data    
*/
#include "config.h"
#include "Maker.h"
#include "Channel.h"
#include "AltroDecoder.h"
#include "Reader.h"
#include <sstream>
#include <iostream>
#include <stdexcept>
# include <rcuxx/Rcu.h>
# include <rcuxx/rcu/RcuACTFEC.h>
# include <rcuxx/Fmd.h>
# include <rcuxx/rcu/RcuACTFEC.h>
# include <rcuxx/fmd/FmdSampleClock.h>
# include <rcuxx/fmd/FmdShiftClock.h>
# include <rcuxx/fmd/FmdRange.h>
# include <rcuxx/fmd/FmdCalIter.h>
# include <rcuxx/fmd/FmdPulser.h>
#endif
#include "DebugGuard.h"

//__________________________________________________________________
FmdDA::Maker::Maker(const Sources_t& src, 
		    const char*      out,
		    int              n, 
		    unsigned int     skip, 
		    unsigned int     baseDDL, 
		    bool             all, 
		    bool             wait)
  : fReader(0),
    fOver(3*32), 
    fFirst(3*32),
    fLast(3*32),
    fIter(3*32),
    fStep(3*32),
    fOffset(14), 
    fExtraOffset(5), 
    fMeter(0), 
    fOutput(out), 
    fCsv(0), 
    fMaxBoard(32),
    fBaseDDL(baseDDL){
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  SetOverSample();
  SetRange();
  SetNSamples();
  SetPulseStep();
  SetOffset();
  SetExtraOffset();
  fReader = new Reader(*this, n, skip);
  if (!fReader) {
    std::stringstream s;
    s << "Maker: No reader made for input ";
    for (size_t i = 0; i < src.size(); i++) 
      s << (i == 0 ? "" : ", ") << (i == src.size() - 1 ? "and " : "")
	<< src[i];
    throw std::runtime_error(s.str());
  }
  fReader->SetInput(src, all, wait);
  std::cout << "Made reader of\n\t";
  for (size_t i = 0; i < src.size(); i++) 
    std::cout << (i == 0 ? "" : ", ") << (i == src.size() - 1 ? "and " : "")
	      << src[i];
  std::cout << std::endl;
}

//__________________________________________________________________
unsigned int
FmdDA::Maker::Address2Index(uint32_t ddl, uint32_t board) const
{
  unsigned int fmdddl = ddl-fBaseDDL;
  if(fmdddl > 2) {
    std::cerr <<"Invalid value of baseDDL : "<<fBaseDDL << std::endl;
    exit(1);
  }
  unsigned int ret = (fmdddl * 32) + board;
  
  return ret;
}

//__________________________________________________________________
unsigned int
FmdDA::Maker::OverSampling(uint32_t ddl, uint32_t board) const
{
  return fOver[Address2Index(ddl, board)];
}
//__________________________________________________________________
unsigned int
FmdDA::Maker::FirstStrip(uint32_t ddl, uint32_t board) const
{
  return fFirst[Address2Index(ddl, board)];
}
//__________________________________________________________________
unsigned int
FmdDA::Maker::LastStrip(uint32_t ddl, uint32_t board) const
{
  return fLast[Address2Index(ddl, board)];
}
//__________________________________________________________________
unsigned int
FmdDA::Maker::NSamples(uint32_t ddl, uint32_t board) const
{
  return fIter[Address2Index(ddl, board)];
}
//__________________________________________________________________
unsigned int
FmdDA::Maker::PulseStep(uint32_t ddl, uint32_t board) const
{
  return fStep[Address2Index(ddl, board)];
}
			    
//__________________________________________________________________
unsigned int
FmdDA::Maker::Timebin2Strip(uint32_t ddl, 
			    uint32_t board, 
			    uint32_t t) const
{
  unsigned int f   = FirstStrip(ddl, board);
  unsigned int o   = OverSampling(ddl, board);
  unsigned int ret = f;
  if (t >= fOffset - fExtraOffset) ret += (t - fOffset - fExtraOffset) / o;
  return ret;
}
  
//__________________________________________________________________
unsigned int
FmdDA::Maker::Timebin2Sample(uint32_t ddl, 
			     uint32_t board, 
			     uint32_t t) const
{
  unsigned int o   = OverSampling(ddl, board);
  unsigned int ret = (t - fOffset - fExtraOffset) % o;
  return ret;
}

//__________________________________________________________________
float 
FmdDA::Maker::Timebin2StripSample(uint32_t ddl, 
				  uint32_t board, 
				  uint32_t t) const
{
  unsigned int f   = FirstStrip(ddl, board);
  unsigned int o   = OverSampling(ddl, board);
  float        tb  = t;
  float        ret = (tb - fOffset - fExtraOffset) / o + f;
  return ret;
}    

//__________________________________________________________________
unsigned
FmdDA::Maker::StripSample2Timebin(uint32_t ddl, 
				  uint32_t board,
				  uint32_t strip, 
				  uint32_t sample) const
{
  unsigned int f   = FirstStrip(ddl, board);
  unsigned int o   = OverSampling(ddl, board);
  return fOffset + fExtraOffset + (strip-f) * o + sample;
}    

//____________________________________________________________________
bool
FmdDA::Maker::CheckHardware(const std::string& url, uint32_t ddl)
{
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
#ifndef HAVE_RCUXX
  return false;
#else
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(url.c_str());
  if (!rcu) return false;
  Rcuxx::Fmd fmd(*rcu);
  Rcuxx::RcuACTFEC* actfec = rcu->ACTFEC();
  if (actfec->Update()) return false;
  if (!actfec) return false;
  for (size_t i = 0; i < 32; i++) {
    if (!actfec->IsOn(i)) continue;
    Rcuxx::FmdSampleClock* sample = fmd.SampleClock();
    Rcuxx::FmdShiftClock*  shift  = fmd.ShiftClock();
    Rcuxx::FmdRange*       range  = fmd.Range();
    Rcuxx::FmdCalIter*     iter   = fmd.CalIter();
    Rcuxx::FmdPulser*      pulser = fmd.Pulser();
    if (!sample || !shift || !range || !iter || !pulser) return false;

    unsigned int ret   = 0;
    unsigned int addr  = Address2Index(ddl, i); // (1 << i);
    fmd.SetAddress(addr);
    if ((ret = sample->Update())) return ret;
    if ((ret = shift->Update()))  return ret;
    if ((ret = range->Update()))  return ret;
    if ((ret = iter->Update()))   return ret;
    if ((ret = pulser->Update())) return ret;
    
    unsigned int over  = shift->Division()/sample->Division();
    unsigned int min   = range->Min();
    unsigned int max   = range->Max();
    unsigned int step  = pulser->Test();
    unsigned int steps = iter->Value();

    fOver[addr]  = over;
    fFirst[addr] = min;
    fLast[addr]  = max;
    fStep[addr]  = step;
    fIter[addr]  = steps;
  }
  return true;
#endif
}

//____________________________________________________________________
void
FmdDA::Maker::SetOverSample(unsigned int over) 
{
  std::fill(fOver.begin(), fOver.end(), over);
}
//____________________________________________________________________
void
FmdDA::Maker::SetRange(unsigned int first, unsigned int last) 
{
  std::fill(fFirst.begin(), fFirst.end(), first);
  std::fill(fLast.begin(), fLast.end(), last);
}
//____________________________________________________________________
void
FmdDA::Maker::SetPulseStep(unsigned int step) 
{
  std::fill(fStep.begin(), fStep.end(), step);
}
//____________________________________________________________________
void
FmdDA::Maker::SetNSamples(unsigned int iter) 
{
  std::fill(fIter.begin(), fIter.end(), iter);
}

//____________________________________________________________________
bool
FmdDA::Maker::Run(const std::string& url)
{
  DGUARD("Run (hw @ %s)", url.c_str());
  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  if (url.empty() || !CheckHardware(url)) 
    std::cerr << "Failed to get values from hardware, using presets"
	      << std::endl;
      
  if (!fReader) {
    std::cerr << "No reader definend " << std::endl;
    return false;
  }
  if (!Start()) return false;
  long ev = 0;

  // Loop over data
  while (true) {
    // Step counters if we need to 
    int  ret  = fReader->GetNextEvent();
    bool stop = false;
    switch (ret) {
    case FmdDA::Reader::kData:       break;
    case FmdDA::Reader::kSkip:       // Fall-through
    case FmdDA::Reader::kNoData:     continue; break;
    case FmdDA::Reader::kEndOfData:  // Fall-through
    case FmdDA::Reader::kMaxReached: // Fall-through
    case FmdDA::Reader::kError:      stop = true; break;
    }
    
    // Increase counter 
    ev++;
    if (!Event()) stop = true;
    // If we're stopped (EOD, Max reached, error) break loop.
    if (stop) break;
  }
  if (!End()) return false;
  return true;
}

//____________________________________________________________________
bool
FmdDA::Maker::Event()
{
  DGUARD("Event");
  fMeter.Step();
  // Get new estimate of events. 
  if (fReader->GetNumberOfEvents() != long(fMeter.N()))
    fMeter.SetN(fReader->GetNumberOfEvents());
  return true;
}

//
// EOF
//
