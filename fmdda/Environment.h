// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:42:16 2006
    @brief   File eXchange Server interface 
    @ingroup fmdda_data    
*/
#ifndef FMDDA_ENVIRONMENT
#define FMDDA_ENVIRONMENT
#ifndef __STRING__
# include  <string>
#endif

namespace FmdDa
{
  /** Singleton object that holds the environment variables. 
      @ingroup fmdda_env
   */
  struct Environment 
  {
    /** Get the singleton
	@return Return reference to singleton. */
    static Environment& Instance();
    
    /** @return true if test mode is on */
    bool               IsTest()   const { return fIsTest; }
    /** @return the runnumber as an unsigned long integer (32bits) */ 
    unsigned long      Run()      const { return fRun; }
    /** @return Path to copy files to */
    const std::string& Path()     const { return fPath; }
    /** @return DAQ role */
    const std::string& Role()     const { return fRole; }
    /** @return Detector code */
    const std::string& Detector() const { return fDetector; }
    /** @return User name */ 
    const std::string& User()     const { return fUser; }
    /** @return User password */
    const std::string& Password() const { return fPassword; }
    /** @return DB host */
    const std::string& Host()     const { return fHost; }
    /** @return DB schema */
    const std::string& Database() const { return fDatabase; }
    /** Read the environment */
    void Read();
  protected:
    /** Check if environment variable @a name is set, and if so return it
	in @a ret 
	@param name Environment variable to check. 
	@param ret  Return value */
    void CheckEnv(const std::string& name, std::string& ret);
    /** Constructor */
    Environment();
    /** Static intance */
    static Environment*  fgInstance;
    /** Whether we're in test mode */
    bool          fIsTest;
    /** Run number */
    unsigned long fRun;
    /** upload path */
    std::string   fPath;
    /** DAQ role */
    std::string   fRole;
    /** Detector code */
    std::string   fDetector;
    /** DAQ log book user name */
    std::string   fUser;
    /** DAQ log book user password */
    std::string   fPassword;
    /** DAQ log book server host */
    std::string   fHost;
    /** DAQ log book database name */
    std::string   fDatabase;
  };
}
#endif
//
// EOF
//


    
